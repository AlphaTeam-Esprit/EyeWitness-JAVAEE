package tn.esprit.client;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.alphateam.eyewitness.persistence.Word;
import tn.esprit.alphateam.eyewitness.services.interfaces.WordServicesRemote;

public class Test {

	public static void main(String[] args) {
		
		try {
			Context ctx = new InitialContext();
			
			WordServicesRemote wordServicesRemote = (WordServicesRemote) ctx.lookup("eyewitness-ear/eyewitness-ejb/WordServices!tn.esprit.alphateam.eyewitness.services.interfaces.WordServicesRemote");
			List<Float> word = wordServicesRemote.analysePhrase("GOOD BAD");
			
			for (Float float1 : word) {
				System.out.println(float1);
			}
			
		} catch (NamingException e) {
			e.printStackTrace();
		}
		

	}

}
