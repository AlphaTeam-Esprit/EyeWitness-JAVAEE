package tn.esprit.bank.services.implementation;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.bank.persistence.Client;
import tn.esprit.bank.services.interfaces.ClientServicesLocal;
import tn.esprit.bank.services.interfaces.ClientServicesRemote;
import tn.esprit.bank.util.MailServiceRemote;

/**
 * Session Bean implementation class ClientServices
 */
@WebService
@Stateless
public class ClientServices implements ClientServicesRemote, ClientServicesLocal {
	@PersistenceContext
	EntityManager entityManager;
	@EJB
	MailServiceRemote mailServiceRemote;

	/**
	 * Default constructor.
	 */
	
	public ClientServices() {
		// TODO Auto-generated constructor stub
	}
	@WebMethod(exclude=true)
	@Override
	public void saveOrUpdateClient(Client client) {
		entityManager.merge(client);

	}
	@WebMethod(exclude=true)
	@Override
	public void deleteClient(Client client) {
		entityManager.remove(entityManager.merge(client));

	}
	@WebMethod(exclude=true)
	@Override
	public Client findClientByCode(String code) {
        Query query;
        query=entityManager.createQuery("SELECT c FROM Client c WHERE c.code = :param");
        query.setParameter("param", code);
		return (Client) query.getSingleResult();
	}
    @WebMethod(operationName="Payment")
    @WebResult(name="Client")
	@Override
	public Boolean makePayment(@WebParam(name="code")String code, @WebParam(name="ccv")Integer ccv, @WebParam(name="name")String WitnessCardName) throws NamingException {
		Client tmpClient = findClientByCode(code);
		if (tmpClient != null)
			if (tmpClient.getBalance() > 5 ) {
				tmpClient.setBalance(tmpClient.getBalance() - 5);
				mailServiceRemote.send(tmpClient.getEmail(), "[EyeWitness]Payment Success",
						"Hello,\nYour payment to get the report of " + WitnessCardName
								+ " witnessCard is done with success\n at the date of " + new Date());
				return true;
			}
		return false;
	}

}
