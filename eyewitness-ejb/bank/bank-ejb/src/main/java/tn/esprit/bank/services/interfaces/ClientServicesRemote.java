package tn.esprit.bank.services.interfaces;

import javax.ejb.Remote;
import javax.naming.NamingException;

import tn.esprit.bank.persistence.Client;

@Remote
public interface ClientServicesRemote {
	void saveOrUpdateClient(Client client);

	void deleteClient(Client client);

	Client findClientByCode(String code);

	Boolean makePayment(String code, Integer ccv, String WitnessCardName)throws NamingException;

}
