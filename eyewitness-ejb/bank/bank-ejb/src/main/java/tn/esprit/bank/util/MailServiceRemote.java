package tn.esprit.bank.util;

import javax.ejb.Remote;
import javax.naming.NamingException;

@Remote
public interface MailServiceRemote {
	void send(String addresses, String topic, String textMessage)throws NamingException ;
}
