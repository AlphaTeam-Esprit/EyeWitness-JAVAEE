package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.Event;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.EventServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.EventServicesRemote;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;

/**
 * Session Bean implementation class EventServices
 */
@Stateless
public class EventServices implements EventServicesRemote, EventServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	@EJB
	UserServicesLocal userServicesLocal;

	public EventServices() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findEventsByWitnessCardId(int id) {

		Query query = entityManager.createQuery("SELECT e FROM Event e WHERE e.witnesscard.id=:id");
		query.setParameter("id", id);

		return query.getResultList();

	}

	@Override
	public void saveOrUpdateEvent(Event event) {
		entityManager.merge(event);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findEventsByAgentId(int agentId) {
		Query query = entityManager.createQuery("SELECT w.events FROM WitnessCard w WHERE w.agent.id=:id");
		query.setParameter("id", agentId);
		return query.getResultList();
	}

	@Override
	public void addEvent(WitnessCard witnessCard, Event event) {

		event.setWitnesscard(witnessCard);
		entityManager.merge(event);
	}

	@Override
	public void deleteEvent(Event event) {
		entityManager.remove(entityManager.merge(event));

	}

	@Override
	public List<Event> listEventsByCurrentAgent(int idAgent) {
		String jpql = "SELECT e FROM Event e,WitnessCard w WHERE w.agent.id=:param and w.id=e.witnesscard";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", idAgent);
		return query.getResultList();

	}

	@Override
	public List<Event> listUnparticipatedEventByUserId(int idUser) {
		User user = userServicesLocal.findUserById(idUser);
		String jpql = "SELECT e FROM Event e WHERE NOT :param MEMBER OF e.users";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", user);
		return query.getResultList();
	}

	@Override
	public List<Event> listParticipatedEventByUserId(int idUser) {
		User user = userServicesLocal.findUserById(idUser);
		String jpql = "SELECT e FROM Event e WHERE :param MEMBER OF e.users";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", user);
		return query.getResultList();
	}

	@Override
	public boolean verifyDateParticipation(Event event, int idUser) {

		List<Event> listParticipatedEventByUserId = listParticipatedEventByUserId(idUser);
		for (Event e : listParticipatedEventByUserId) {
			if (e.getDateFrom().compareTo(event.getDateFrom()) == 0 && e.getDateTo().compareTo(event.getDateTo()) == 0)
				return false;

		}
		return true;

	}

	@Override
	public void participate(int idUser, Event event) {
		User user = userServicesLocal.findUserById(idUser);
		List<Event> events = user.getEvents();
		events.add(event);
		user.setEvents(events);
		userServicesLocal.register(user);

	}

	@Override
	public List<User> listFollowedUsersByUserId(int idUser) {
		User user = userServicesLocal.findUserById(idUser);

		String jpql = "SELECT u FROM User u WHERE :param MEMBER OF u.followers  ";

		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", user);

		return query.getResultList();
	}

	@Override
	public List<User> listFollowedUsersByEvent(int idEvent, int idUser) {
		Event event = findEventById(idEvent);
		List<User> usersfinal = new ArrayList<>();
		List<User> users = listFollowedUsersByUserId(idUser);
		for (User user : users) {
			for (Event eventitem : user.getEvents()) {

				if (eventitem == event)
					usersfinal.add(user);
			}
		}
		return usersfinal;
	}

	@Override
	public Event findEventById(int id) {

		return entityManager.find(Event.class, id);

	}

	@Override
	public List<Event> listEvents() {
		String jpql = "SELECT e FROM Event e ";
		Query query = entityManager.createQuery(jpql);
		return query.getResultList();
	}

	@Override
	public HashMap<Integer, WitnessCard> numberOfParticipatedUserToEventWitnesscard() {
		HashMap<Integer, WitnessCard> hashmap = new HashMap<>();
		List<Event> events = listEvents();
		for (Event event : events) {

			hashmap.put(event.getUsers().size(), event.getWitnesscard());
		}
		return hashmap;
	}

}
