package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.BadWord;
import tn.esprit.alphateam.eyewitness.persistence.Comments;
import tn.esprit.alphateam.eyewitness.persistence.UserReport;
import tn.esprit.alphateam.eyewitness.services.interfaces.BadWordsServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.BadWordsServicesRemote;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;

/**
 * Session Bean implementation class BadWordsServices
 */
@Stateless
public class BadWordsServices implements BadWordsServicesRemote, BadWordsServicesLocal {

	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private UserServicesLocal userServicesLocal;

	/**
	 * Default constructor.
	 */
	public BadWordsServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addBadWord(BadWord badword) {
		entityManager.merge(badword);

	}

	@Override
	public void deleteBadWord(BadWord badWord) {
		entityManager.remove(entityManager.merge(badWord));

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BadWord> getAllBadWords() {

		String jpql = "SELECT w FROM BadWord w ";
		Query query = entityManager.createQuery(jpql);

		return query.getResultList();

	}

	@Override
	public Boolean CheckComment(String comment) {
		
		List<String> badwordsText = getAllBadWordsText();
		Boolean isbadcomment = false;
		
		String str[] = comment.trim().split("\\s+");

		int i = 0;
		while ((isbadcomment == false) && (i < str.length)) {
			if (badwordsText.contains(str[i])){
				isbadcomment = true;}
			   i++;
		
				
		}
		return isbadcomment;
	}

	@Override
	public boolean CommentAwitnessCard(Comments comment) {
		boolean commentInserted = false;
		String commentText = comment.getComment();
		if (CheckComment(commentText)) {
			// il contient des bad words

			UserReport userReport = new UserReport();
			userReport.setIdReported(comment.getIdUser());
			userReport.setIdReporting(0);
			userReport.setReason("bad Word");
			userServicesLocal.reportUserById(userReport);
		} else {
			commentInserted = true;
			entityManager.merge(comment);

		}

		return commentInserted;

	}

	@Override
	public void deleteBadWordByid(int idBadWord) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getAllBadWordsText() {
	List <String> badWordsText = new ArrayList<String>();
	List<BadWord> badwords = getAllBadWords();
	for (BadWord badWord : badwords) {
		badWordsText.add(badWord.getText());
	}
	
		return badWordsText;
	}

	

}
