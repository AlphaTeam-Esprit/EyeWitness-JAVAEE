package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.Notification;

@Local
public interface NotificationServicesLocal {
	void saveNotification(Notification n);
	List<Notification> findFiveLastNotificationsByUserId(Integer userId);
}
