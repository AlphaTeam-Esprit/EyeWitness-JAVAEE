package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entity implementation class for Entity: AgentRequest
 *
 */
@Entity

public class AgentRequest implements Serializable {
	@EmbeddedId
	
	private AgentRequestId agentRequestId;
	@ManyToOne
	@JoinColumn(name = "witnessCardId", referencedColumnName = "id", updatable = false, insertable = false)
	private WitnessCard witnesscard;
	public WitnessCard getWitnesscard() {
		return witnesscard;
	}


	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}


	public User getAgent() {
		return agent;
	}


	public void setAgent(User agent) {
		this.agent = agent;
	}

	@ManyToOne
	@JoinColumn(name = "agentId", referencedColumnName = "id", updatable = false, insertable = false)
	private User agent;
	private String message;
	private String status;
	private static final long serialVersionUID = 1L;

	public AgentRequest() {
		super();
	}   
	
	
	public AgentRequestId getAgentRequestId() {
		return agentRequestId;
	}


	public void setAgentRequestId(AgentRequestId agentRequestId) {
		this.agentRequestId = agentRequestId;
	}


	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
   
}
