package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the subscriptionswcs database table.
 * 
 */
@Entity
@Table(name="subscriptionswcs")
@NamedQuery(name="Subscriptionswc.findAll", query="SELECT s FROM Subscriptionswc s")
public class Subscriptionswc implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SubscriptionswcPK id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date subscriptionDate;

	@Lob
	private String type;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="UserId")
	private User user;

	//bi-directional many-to-one association to Witnesscard
	@ManyToOne
	@JoinColumn(name="WitnessCardId")
	private WitnessCard witnesscard;

	public Subscriptionswc() {
	}

	public SubscriptionswcPK getId() {
		return this.id;
	}

	public void setId(SubscriptionswcPK id) {
		this.id = id;
	}

	public Date getSubscriptionDate() {
		return this.subscriptionDate;
	}

	public void setSubscriptionDate(Date subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public WitnessCard getWitnesscard() {
		return this.witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

}