package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the categories database table.
 * 
 */
@Entity
@Table(name="categories")
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String name;

	@Lob
	private String status;

	//bi-directional many-to-one association to Choosencriteria
	@OneToMany(mappedBy="category", fetch=FetchType.EAGER)
	private List<Choosencriteria> choosencriterias;

	//bi-directional many-to-one association to Witnesscard
	@OneToMany(mappedBy="category", fetch=FetchType.EAGER)
	private List<WitnessCard> witnesscards;

	public Category() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Choosencriteria> getChoosencriterias() {
		return this.choosencriterias;
	}

	public void setChoosencriterias(List<Choosencriteria> choosencriterias) {
		this.choosencriterias = choosencriterias;
	}

	public Choosencriteria addChoosencriteria(Choosencriteria choosencriteria) {
		getChoosencriterias().add(choosencriteria);
		choosencriteria.setCategory(this);

		return choosencriteria;
	}

	public Choosencriteria removeChoosencriteria(Choosencriteria choosencriteria) {
		getChoosencriterias().remove(choosencriteria);
		choosencriteria.setCategory(null);

		return choosencriteria;
	}

	public List<WitnessCard> getWitnesscards() {
		return this.witnesscards;
	}

	public void setWitnesscards(List<WitnessCard> witnesscards) {
		this.witnesscards = witnesscards;
	}

	public WitnessCard addWitnesscard(WitnessCard witnesscard) {
		getWitnesscards().add(witnesscard);
		witnesscard.setCategory(this);

		return witnesscard;
	}

	public WitnessCard removeWitnesscard(WitnessCard witnesscard) {
		getWitnesscards().remove(witnesscard);
		witnesscard.setCategory(null);

		return witnesscard;
	}

}