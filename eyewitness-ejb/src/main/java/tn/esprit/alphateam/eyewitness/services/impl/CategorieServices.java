package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.Category;

import tn.esprit.alphateam.eyewitness.services.interfaces.CategorieServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.CategorieServicesRemote;

/**
 * Session Bean implementation class CategorieServices
 */
@Stateless
public class CategorieServices implements CategorieServicesRemote, CategorieServicesLocal {
	@PersistenceContext
	EntityManager entityManager;
    public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
     * Default constructor. 
     */
    public CategorieServices() {
        // TODO Auto-generated constructor stub
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findCategoriesByStatus() {
		String jpql="SELECT c FROM Category c WHERE c.status=:param";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "STANDBY");
		return query.getResultList();
	}

	@Override
	public void validateCategorie(Integer id) {
	
		String jpql="UPDATE Category c set c.status=:param WHERE c.id=:param1";
		Query query= entityManager.createQuery(jpql);
		query.setParameter("param", "VALID");
		query.setParameter("param1", id);
		query.executeUpdate();
		
		
	}

	@Override
	public void deleteCategorie(Category category) {
		entityManager.remove(entityManager.merge(category));
		
	}

}
