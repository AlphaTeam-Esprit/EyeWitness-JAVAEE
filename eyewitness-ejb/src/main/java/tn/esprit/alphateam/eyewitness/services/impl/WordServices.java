package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.persistence.Word;
import tn.esprit.alphateam.eyewitness.services.interfaces.WordServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WordServicesRemote;

/**
 * Session Bean implementation class WordServices
 */
@Stateless
public class WordServices implements WordServicesRemote, WordServicesLocal {
	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public WordServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void saveAndUpdate(Word word) {
		entityManager.merge(word);

	}

	@Override
	public Word FindWordByMeaning(String meaning) {
		Query query;
		String jpql = "SELECT w FROM Word WHERE w.meaning = :param";
		query = entityManager.createQuery(jpql);
		query.setParameter("param", meaning);
		return (Word) query.getSingleResult();
	}

	@Override
	public void removeWord(Word word) {
		entityManager.remove(entityManager.merge(word));
	}

	@Override
	public Word FindWordByName(String name) {
        Word word =null;
		//List<Word> words = new ArrayList<Word>();
		System.out.println("Name : " + name);
		Query query = entityManager.createQuery("SELECT u FROM Word u WHERE u.name=:p");
		query.setParameter("p", name);
		//words = (List<Word>) query.getResultList();
		//System.out.println("Size : " + words.size());
		try {
			return (Word) query.getSingleResult();		
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Float> analysePhrase(String phrase) {
		int firstChar = -1;
		int length = 0;
		Float pos = (float) 0;
		Float neg = (float) 0;
		Float neu = (float) 0;
		Float total = (float) 0;
		List<String> listWords = new ArrayList<String>();

		for (int i = 0; i < phrase.length(); i++) {
			if (!Character.isLetter(phrase.charAt(i))) {
				if (firstChar != -1)
					listWords.add(phrase.substring(firstChar, firstChar + length));
				firstChar = -1;
				length = 0;
			} else {
				if (firstChar == -1)
					firstChar = i;
				length++;
			}
			if (firstChar != -1 && i == phrase.length() - 1)
				listWords.add(phrase.substring(firstChar, firstChar + length));
		}
		for (String w : listWords) {
			String tmp = w.toUpperCase();
			try {
				if (FindWordByName(tmp).getMeaning().contains("P")) {
					pos = pos + 1;
					System.out.println("pos:"+pos);
				} else if (FindWordByName(tmp).getMeaning().contains("N")) {
					neg = neg + 1;
				} else if (FindWordByName(tmp).getMeaning().contains("M")) {
					neu = neu + 1;
				}
			}catch(Exception e) {
				System.out.println("Exception occured in loop !");
				continue;
			}

		}
		total = pos + neg + neu;
		List<Float> result = new ArrayList<Float>();
		result.add((pos * 100) / total);
		result.add((neg * 100) / total);
		result.add((neu * 100) / total);
		for (Float f : result) {
			System.out.println("res:"+f);
		}
		
		return result;
	}

	@Override
	public List<Float> analyseWitnessCard(Integer idWitnessCard) {
		Query query;
		List<String> Comments = new ArrayList<String>();
		List<Float> percentage = new ArrayList<Float>();

		String jpql = "SELECT w.comment FROM Evaluation w WHERE w.witnesscard.id = :param";
		
		query = entityManager.createQuery(jpql);
		query.setParameter("param", idWitnessCard);
		Comments = (List<String>) query.getResultList();
		System.out.println(Comments.size());
		for (String c : Comments) {
			List<Float> p = new ArrayList<Float>();
			System.out.println("In loop");
			if (c != null) {
				p = analysePhrase(c);
				System.out.println("p size : " + p.size());
				if (percentage.isEmpty()) {
					if (!(p.get(0).equals(0)) && !(p.get(1).equals(0)) && !(p.get(2).equals(0)))
						percentage = p;
					System.out.println("Percentage empty");
				} else {
					System.out.println("Else not empty");
					if (!(p.get(0).equals(0)) && !(p.get(1).equals(0)) && !(p.get(2).equals(0))) {
						
						for (int i = 0; i < 3; i++) {
							Float tmp = (Float) ((p.get(i) + percentage.get(i)) / 2);
							System.out.println("tmp:" + tmp);
							percentage.add(i, tmp);
						}

					}

				}
			}
		}
		System.out.println("percent size : " + percentage.size());
		return percentage;
	}

	@Override
	public List<Word> getAllWords() {

		Query query = entityManager.createQuery("SELECT w FROM Word w");

		return (List<Word>) query.getResultList();
	}

	@Override
	public WitnessCard getWitnessCard(Integer idWitnessCard) {
		
		return entityManager.find(WitnessCard.class, idWitnessCard);
	}

}
