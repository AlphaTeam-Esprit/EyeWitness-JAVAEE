package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.persistence.Word;

@Local
public interface WordServicesLocal {
	void saveAndUpdate(Word word);

	Word FindWordByMeaning(String meaning);

	Word FindWordByName(String name);

	void removeWord(Word word);

	List<Float> analysePhrase(String phrase);

	List<Float> analyseWitnessCard(Integer idWitnessCard);

	List<Word> getAllWords();

	WitnessCard getWitnessCard(Integer idWitnessCard);
}
