package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.Notification;
import tn.esprit.alphateam.eyewitness.services.interfaces.NotificationServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.NotificationServicesRemote;

/**
 * Session Bean implementation class NotificationServices
 */
@Stateless
public class NotificationServices implements NotificationServicesRemote, NotificationServicesLocal {
	
	@PersistenceContext
	EntityManager entityManager;

    public NotificationServices() {
    }

	@Override
	public void saveNotification(Notification n) {
		entityManager.persist(n);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> findFiveLastNotificationsByUserId(Integer userId) {
		Query query = entityManager.createQuery("SELECT n FROM Notification n WHERE n.user.id=:id ORDER BY n.date DESC");
		query.setParameter("id", userId);
		query.setMaxResults(5);
		return query.getResultList();
	}
	
}
