package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: UserReport
 *
 */
@Entity

public class UserReport implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idReport;
	private Integer idReported;
	private Integer idReporting;
	private String reason;
	private static final long serialVersionUID = 1L;

	public UserReport() {
		super();
	}   
	public Integer getIdReport() {
		return this.idReport;
	}

	public void setIdReport(Integer idReport) {
		this.idReport = idReport;
	}   
	public Integer getIdReported() {
		return this.idReported;
	}

	public void setIdReported(Integer idReported) {
		this.idReported = idReported;
	}   
	public Integer getIdReporting() {
		return this.idReporting;
	}

	public void setIdReporting(Integer idReporting) {
		this.idReporting = idReporting;
	}   
	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
   
}
