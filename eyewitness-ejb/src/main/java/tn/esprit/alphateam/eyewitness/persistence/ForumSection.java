package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ForumSection implements Serializable {
	private static final long serialVersionUID = 3424027786374089658L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String title;
	private String description;
	private String icon;

	@OneToMany(mappedBy = "forumSection", fetch=FetchType.EAGER)
	private List<ForumSubject> forumSubjects;

	public ForumSection() {
		super();
	}

	public List<ForumSubject> getForumSubjects() {
		return forumSubjects;
	}

	public void setForumSubjects(List<ForumSubject> forumSubjects) {
		this.forumSubjects = forumSubjects;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
