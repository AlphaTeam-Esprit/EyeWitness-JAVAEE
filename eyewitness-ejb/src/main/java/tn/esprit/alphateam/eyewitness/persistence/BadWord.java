package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: BadWord
 *
 */
@Entity

public class BadWord implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idBadWord;
	private String text;
	private static final long serialVersionUID = 1L;

	public BadWord() {
		super();
	}   
	public Integer getIdBadWord() {
		return this.idBadWord;
	}

	public void setIdBadWord(Integer idBadWord) {
		this.idBadWord = idBadWord;
	}   
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}
   
}
