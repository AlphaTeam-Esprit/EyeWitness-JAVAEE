package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.AgentRequest;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.UserReport;

@Remote
public interface UserServicesRemote {

	User login(String email, String password);
	void register(User user);
	User findUserById(Integer id);
	void sendEmailToUser(String textMessage, String subject, String userEmail);
	public void sendEmailToUser(String textMessage, String subject, String userEmail, String base64Image);
	
	void banUser(Integer id, int durationDays);
	List<User> findBannableUsers();
	List<User> findBannedUsers();
	void unbanUser(Integer id);
	void assignAgent(AgentRequest agentRequest);
	void deleteAgentRequest(Integer witnessCardId);
	List<AgentRequest>findAgentRequest();
	List<AgentRequest>findAgentRequestActive();
	void deleteAgentRequestBywitnessCardIdAndAgentId(Integer witnessCardId,Integer agentId);
	void demoteAgent(AgentRequest agentRequest);
	int getReportingByIdUserReported(int idUserReported);
	void reportUserById(UserReport userReport);
	void BanBadUserById(int idUser);
	void incrementBadWords(int idbadUser);
	
	List<User>findAgents();
}
