package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.Category;



@Local
public interface CategorieServicesLocal {
    
	List<Category> findCategoriesByStatus();
	void validateCategorie(Integer id);
	void deleteCategorie(Category category);
}
