package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.Competition;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;

@Remote
public interface CompetitionsServicesRemote {
	public List<WitnessCard> findWitnessCards();

	public List<Competition> findAllCompetitions();

	public Competition findCompetitionById(int id);

	public void createCompetition(Competition competition);

	public void deleteCompetition(Competition competition);

	public void updateCompetition(Competition competition);

}
