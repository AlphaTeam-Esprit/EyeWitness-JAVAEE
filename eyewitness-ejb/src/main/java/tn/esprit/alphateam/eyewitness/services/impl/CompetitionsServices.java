package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.alphateam.eyewitness.persistence.Competition;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.CompetitionsServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.CompetitionsServicesRemote;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;

/**
 * Session Bean implementation class CompetitionsServices
 */
@Stateless
@LocalBean
public class CompetitionsServices implements CompetitionsServicesRemote, CompetitionsServicesLocal {
	@PersistenceContext
	EntityManager em;
	@EJB
	UserServicesLocal userserviceslocal;
	public List<Competition> competitions = new ArrayList<Competition>();
	public List<WitnessCard> witnessCard = new ArrayList<WitnessCard>();

	/**
	 * Default constructor.
	 */
	public CompetitionsServices() {
	}

	@Override
	public List<Competition> findAllCompetitions() {
		return em.createQuery("select c from Competition c", Competition.class).getResultList();

	}

	@Override
	public Competition findCompetitionById(int id) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Competition> query = cb.createQuery(Competition.class);
			Root<Competition> competition = query.from(Competition.class);
			query.where(cb.equal(competition.get("id"), id));
			return em.createQuery(query).getSingleResult();

		} catch (NoResultException nre) {
			return null;

		} catch (NonUniqueResultException nure) {
			return null;
		}

	}

	@Override
	public void createCompetition(Competition competition) {
		em.persist(competition);
	}

	@Override
	public void deleteCompetition(Competition compe) {
		Competition competition = findCompetitionById(compe.getId());
		em.remove(competition);

	}

	@Override
	public void updateCompetition(Competition compe) {
		Competition competition = findCompetitionById(compe.getId());

		competition.setDateFrom(compe.getDateFrom());
		competition.setDateTo(compe.getDateTo());
		competition.setDescripiton(compe.getDescripiton());
		competition.setTheme(compe.getTheme());

		em.persist(competition);
		em.flush();

	}

	@Override
	public List<WitnessCard> findWitnessCards() {
		String jpql = "SELECT c FROM Witnesscard c";
		Query query = em.createQuery(jpql);
		return query.getResultList();
	}

}
