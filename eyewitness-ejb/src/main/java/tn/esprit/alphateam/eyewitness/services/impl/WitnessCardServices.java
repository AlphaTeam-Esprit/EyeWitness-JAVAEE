package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.Comments;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesRemote;
import tn.esprit.alphateam.eyewitness.statistics.NumberOfSubscribersByDate;
import tn.esprit.alphateam.eyewitness.statistics.NumberOfVisitorsByDate;
import tn.esprit.alphateam.eyewitness.statistics.RatingByWitnessCard;

/**
 * Session Bean implementation class WitnessCardServices
 */
@Stateless
public class WitnessCardServices implements WitnessCardServicesRemote, WitnessCardServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	public WitnessCardServices() {
	}

	@Override
	public void saveOrUpdateWitnessCard(WitnessCard c) {
		entityManager.merge(c);
	}
	@Override
	public List<WitnessCard> listWitnessCardsByCurrentAgent(int idAgent) {
		String jpql="SELECT w FROM WitnessCard w WHERE w.agent.id=:param";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", idAgent);
		return query.getResultList();
	}
	@Override
	public WitnessCard findWitnesscardById(Integer id) {
		return entityManager.find(WitnessCard.class, id);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public List<NumberOfSubscribersByDate> findNumberOfSubscribersByDateByWitnessCardId(Integer cardId) {
		List<NumberOfSubscribersByDate> res = new ArrayList<>();
		List queryResult = new ArrayList();

		Query query = entityManager.createQuery(
				"SELECT s.subscriptionDate, count(s) " + "FROM Subscriptionswc s " + "WHERE s.witnesscard.id=:cardId "
						+ "GROUP BY DAY(s.subscriptionDate) " + "ORDER BY DAY(s.subscriptionDate)");
		query.setParameter("cardId", cardId);
		queryResult = query.getResultList();

		for (Object o : queryResult) {
			Object[] row = (Object[]) o;
			Date date = (Date) row[0];
			long nbSubscribers = (long) row[1];
			NumberOfSubscribersByDate stat = new NumberOfSubscribersByDate(date, nbSubscribers);
			res.add(stat);
		}

		return res;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<NumberOfVisitorsByDate> findNumberOfVisitorByDateByWitnessCardId(Integer cardId) {
		List<NumberOfVisitorsByDate> result = new ArrayList<>();
		List queryResult = new ArrayList();
		Query query = entityManager.createQuery("SELECT s.dateStatistic, s.numberOfVisitors "
				+ "FROM WitnessCardStatistic s " + "WHERE s.witnesscard.id=:cardId");
		query.setParameter("cardId", cardId);
		queryResult = query.getResultList();

		for (Object o : queryResult) {
			Object[] row = (Object[]) o;
			Date dateOfStatistic = (Date) row[0];
			int numberOfVisitors = (int) row[1];
			result.add(new NumberOfVisitorsByDate(numberOfVisitors, dateOfStatistic));

		}

		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<RatingByWitnessCard> findNumberOfRatingsByWitnessCardId(Integer cardId) {
		List<RatingByWitnessCard> result = new ArrayList<>();
		List queryResult = new ArrayList();

		Query query = entityManager.createQuery("SELECT count(r.rating), r.rating " + "FROM WitnessCardRating r "
				+ "WHERE r.witnesscard.id=:cardId " + "GROUP BY r.rating");
		query.setParameter("cardId", cardId);
		queryResult = query.getResultList();

		for (Object o : queryResult) {
			Object[] row = (Object[]) o;
			long number = (long) row[0];
			int rating = (int) row[1];
			result.add(new RatingByWitnessCard(rating, number));
		}

		return result;
	}

	@Override
	public List<WitnessCard> listWitnessCards() {

		return entityManager.createQuery("select w from WitnessCard w", WitnessCard.class).getResultList();
	}

	@Override
	public Long getCommentCount(int id) {
		Query query = entityManager.createQuery("SELECT COUNT(e) FROM Evaluation e WHERE e.id.witnessCardId = :id");

		query.setParameter("id", id);

		return (Long) query.getSingleResult();
	}

	@Override
	public void add(WitnessCard wc) {
		entityManager.merge(wc);

	}

	@Override
	public WitnessCard findWitnessById(Integer idwc) {
		return entityManager.find(WitnessCard.class, idwc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WitnessCard> findNoValidWitnessCard() {

		String jpql = "SELECT wc FROM WitnessCard wc WHERE wc.status=:param";

		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "STANDBY");
		return query.getResultList();

	}

	@Override
	public void validateWitnessCard(int id) {

		String jpql = "UPDATE WitnessCard wc set status =:param where wc.id=:param1 ";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "VALID");
		query.setParameter("param1", id);
		query.executeUpdate();

	}

	

	@SuppressWarnings("unchecked")
	@Override
	public List<WitnessCard> findWitnessCardsByAgent(Integer AgentId) {

		String jpql = "SELECT wc FROM WitnessCard wc WHERE wc.agent.id=:param";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", AgentId);
		return query.getResultList();
	}



	@Override
	public WitnessCard findWitnessCardById(Integer id) {
		return entityManager.find(WitnessCard.class, id);
	}

	@Override
	public void DeleteWitnessCard(WitnessCard WitnessCard) {
		entityManager.remove(entityManager.merge(WitnessCard));
		
	}
	
	@Override
	public List<WitnessCard> getAllWitnessCardsByname(String name) {

		String jpql = "SELECT w FROM WitnessCard w  where w.name  like '%"+name+"%'";
		Query query = entityManager.createQuery(jpql);
		return query.getResultList();

	}

	@Override
	public List<Comments> getAllCommentsByWitnessCardId(int idWitnessCard) {
		String jpql = "SELECT c FROM Comments c  where c.idWitnessCard =:param1";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param1", idWitnessCard);
		return query.getResultList();

	}

	@Override
	public List<WitnessCard> getAllWitnessCard() {
		String jpql = "SELECT w FROM WitnessCard w ";
		Query query = entityManager.createQuery(jpql);
		return query.getResultList();
	}

	@Override
	public WitnessCard getWitnessCardByid(int idwitnessCard) {
		return entityManager.find(WitnessCard.class, idwitnessCard);
	}

}
