package tn.esprit.alphateam.eyewitness.util;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.alphateam.eyewitness.persistence.Category;

/**
 * Session Bean implementation class DatabasePopulator
 */
@Startup
@Singleton
@LocalBean
public class DatabasePopulator {

	@PersistenceContext
	EntityManager entityManager;
	
    public DatabasePopulator() {
    }
    
    @PostConstruct
    public void init() {
    	Category c = new Category();
    	c.setName("My category");
    	entityManager.persist(c);
    }

}
