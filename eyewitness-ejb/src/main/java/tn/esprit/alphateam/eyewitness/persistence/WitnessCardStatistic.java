package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: WitnessCardStatistic
 *
 */
@Entity
public class WitnessCardStatistic implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Temporal(TemporalType.DATE)
	private Date dateStatistic;
	private Integer numberOfVisitors;
	
	@ManyToOne
	private WitnessCard witnesscard;
	 
	private static final long serialVersionUID = 1L;

	public WitnessCardStatistic() {
		super();
	}   
	
	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Date getDateStatistic() {
		return this.dateStatistic;
	}

	public void setDateStatistic(Date dateStatistic) {
		this.dateStatistic = dateStatistic;
	}   
	public Integer getNumberOfVisitors() {
		return this.numberOfVisitors;
	}

	public void setNumberOfVisitors(Integer numberOfVisitors) {
		this.numberOfVisitors = numberOfVisitors;
	}
   
}
