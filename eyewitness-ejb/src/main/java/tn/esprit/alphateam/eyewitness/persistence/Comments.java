package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Comments
 *
 */
@Entity

public class Comments implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdComment;
	private Integer idWitnessCard;
	private Integer idUser;
	private String UserName;
	private String Comment;
	private static final long serialVersionUID = 1L;

	public Comments() {
		super();
	}   
	public Integer getIdComment() {
		return this.IdComment;
	}

	public void setIdComment(Integer IdComment) {
		this.IdComment = IdComment;
	}   
	public Integer getIdWitnessCard() {
		return this.idWitnessCard;
	}

	public void setIdWitnessCard(Integer idWitnessCard) {
		this.idWitnessCard = idWitnessCard;
	}   
	public Integer getIdUser() {
		return this.idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}   
	public String getUserName() {
		return this.UserName;
	}

	public void setUserName(String UserName) {
		this.UserName = UserName;
	}   
	public String getComment() {
		return this.Comment;
	}

	public void setComment(String Comment) {
		this.Comment = Comment;
	}
   
}
