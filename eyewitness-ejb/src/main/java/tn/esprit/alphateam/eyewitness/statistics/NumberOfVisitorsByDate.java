package tn.esprit.alphateam.eyewitness.statistics;

import java.io.Serializable;
import java.util.Date;

public class NumberOfVisitorsByDate implements Serializable{
	private static final long serialVersionUID = -3206157870009667638L;
	
	private int numberOfVisitors;
	private Date date;

	public NumberOfVisitorsByDate(int numberOfVisitors, Date date) {
		super();
		this.numberOfVisitors = numberOfVisitors;
		this.date = date;
	}

	public int getNumberOfVisitors() {
		return numberOfVisitors;
	}

	public void setNumberOfVisitors(int numberOfVisitors) {
		this.numberOfVisitors = numberOfVisitors;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "NumberOfVisitorsByDate [numberOfVisitors=" + numberOfVisitors + ", date=" + date + "]";
	}
	
}
