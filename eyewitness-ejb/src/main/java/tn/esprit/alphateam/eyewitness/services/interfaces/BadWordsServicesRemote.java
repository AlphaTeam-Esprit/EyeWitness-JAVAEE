package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.BadWord;
import tn.esprit.alphateam.eyewitness.persistence.Comments;

@Remote
public interface BadWordsServicesRemote {
	
	public void addBadWord(BadWord badword ) ; 
	public void  deleteBadWord(BadWord badWord);
	
	public List<BadWord> getAllBadWords();
	public List<String> getAllBadWordsText();
	public Boolean CheckComment(String comment);
	public boolean CommentAwitnessCard(Comments comment);
    public void deleteBadWordByid(int idBadWord );
    
}
