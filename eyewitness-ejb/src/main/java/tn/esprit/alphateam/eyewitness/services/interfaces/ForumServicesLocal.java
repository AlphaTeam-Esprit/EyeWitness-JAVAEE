package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.ForumMessage;
import tn.esprit.alphateam.eyewitness.persistence.ForumReport;
import tn.esprit.alphateam.eyewitness.persistence.ForumSection;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic;
import tn.esprit.alphateam.eyewitness.persistence.User;

@Local
public interface ForumServicesLocal {

	List<ForumSection> findAllSections();

	List<ForumTopic> findTopicsBySubjectId(int subjectId);

	List<ForumSubject> findSubjectsBySectionId(int sectionId);

	List<ForumTopic> findHotTopics();

	List<User> findTopPosters();

	List<ForumTopic> findMostNotoriousTopics();

	void saveOrUpdateTopic(ForumTopic topic);

	void deleteTopic(ForumTopic topic);

	ForumTopic findTopicById(int topicId);

	List<ForumMessage> findMessagesByTopicId(int topicId);

	void saveOrUpdateMessage(ForumMessage message);

	void reportMessage(ForumReport report);

	List<ForumReport> findAllReports();
	
	void saveOrUpdateSection(ForumSection section);
	
	int findNumberOfMessagesBySubjectId(int subjectId);
	
	ForumSubject findSubjectById(int subjectId);
	
	ForumTopic findLatestTopicBySubjectId(int subjectId);
	
	List<ForumMessage> findMessagesByTopicIdOrderByDate(int topicId);
	
	void saveOrUpdateSubject(ForumSubject subject);
	
	ForumSection findSectionById(int idSection);
}
