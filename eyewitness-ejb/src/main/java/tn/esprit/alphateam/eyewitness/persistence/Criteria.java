package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the criterias database table.
 * 
 */
@Entity
@Table(name="criterias")
@NamedQuery(name="Criteria.findAll", query="SELECT c FROM Criteria c")
public class Criteria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String name;

	//bi-directional many-to-one association to Choosencriteria
	@OneToMany(mappedBy="criteria", fetch=FetchType.EAGER)
	private List<Choosencriteria> choosencriterias;

	public Criteria() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Choosencriteria> getChoosencriterias() {
		return this.choosencriterias;
	}

	public void setChoosencriterias(List<Choosencriteria> choosencriterias) {
		this.choosencriterias = choosencriterias;
	}

	public Choosencriteria addChoosencriteria(Choosencriteria choosencriteria) {
		getChoosencriterias().add(choosencriteria);
		choosencriteria.setCriteria(this);

		return choosencriteria;
	}

	public Choosencriteria removeChoosencriteria(Choosencriteria choosencriteria) {
		getChoosencriterias().remove(choosencriteria);
		choosencriteria.setCriteria(null);

		return choosencriteria;
	}

}