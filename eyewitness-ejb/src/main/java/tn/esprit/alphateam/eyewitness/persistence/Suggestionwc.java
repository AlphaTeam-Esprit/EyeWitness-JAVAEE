package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the suggestionwcs database table.
 * 
 */
@Entity
@Table(name="suggestionwcs")
@NamedQuery(name="Suggestionwc.findAll", query="SELECT s FROM Suggestionwc s")
public class Suggestionwc implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SuggestionwcPK id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateSuggestion;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="SuggestedUserId")
	private User suggestingUser;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="SuggestingUserId")
	private User suggestedUser;

	//bi-directional many-to-one association to Witnesscard
	@ManyToOne
	@JoinColumn(name="SuggestedCardId")
	private WitnessCard witnesscard;

	public Suggestionwc() {
	}

	public SuggestionwcPK getId() {
		return this.id;
	}

	public void setId(SuggestionwcPK id) {
		this.id = id;
	}

	public Date getDateSuggestion() {
		return this.dateSuggestion;
	}

	public void setDateSuggestion(Date dateSuggestion) {
		this.dateSuggestion = dateSuggestion;
	}

	public User getSuggestingUser() {
		return this.suggestingUser;
	}

	public void setSuggestingUser(User suggestingUser) {
		this.suggestingUser = suggestingUser;
	}

	public User getSuggestedUser() {
		return this.suggestedUser;
	}

	public void setSuggestedUser(User suggestedUser) {
		this.suggestedUser = suggestedUser;
	}

	public WitnessCard getWitnesscard() {
		return this.witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

}