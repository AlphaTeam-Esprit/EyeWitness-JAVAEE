package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class WitnessCardRatingId implements Serializable {
	private static final long serialVersionUID = -4120091781551553516L;
	
	private Integer idWitnessCard;
	private Integer idUser;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		result = prime * result + ((idWitnessCard == null) ? 0 : idWitnessCard.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WitnessCardRatingId other = (WitnessCardRatingId) obj;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		if (idWitnessCard == null) {
			if (other.idWitnessCard != null)
				return false;
		} else if (!idWitnessCard.equals(other.idWitnessCard))
			return false;
		return true;
	}

	public WitnessCardRatingId() {
		super();
	}

	public Integer getIdWitnessCard() {
		return idWitnessCard;
	}

	public void setIdWitnessCard(Integer idWitnessCard) {
		this.idWitnessCard = idWitnessCard;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

}
