package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.Evaluation;

@Local
public interface EvaluationServicesLocal {
	List<Evaluation> findAll();
}
