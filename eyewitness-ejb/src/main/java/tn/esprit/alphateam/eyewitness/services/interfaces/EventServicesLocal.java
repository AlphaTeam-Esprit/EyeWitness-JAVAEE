package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.Event;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;

@Local
public interface EventServicesLocal {
	List<Event> findEventsByWitnessCardId(int id);
	Event findEventById(int id);
	void saveOrUpdateEvent(Event event);
	List<Event> findEventsByAgentId(int agentId);
	void addEvent(WitnessCard witnessCard, Event event);
	void deleteEvent(Event event);
	List<Event> listEventsByCurrentAgent(int idAgent);
	List<Event> listUnparticipatedEventByUserId(int idUser);
	List<Event> listParticipatedEventByUserId(int idUser);
	boolean verifyDateParticipation(Event event, int idUser);
	void participate(int idUser, Event event);
	List<User> listFollowedUsersByUserId(int idUser);
	List<User> listFollowedUsersByEvent(int idEvent, int idUser);
	List<Event> listEvents();
	HashMap<Integer, WitnessCard> numberOfParticipatedUserToEventWitnesscard();
}
