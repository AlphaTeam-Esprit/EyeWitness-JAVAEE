package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the suggestionwcs database table.
 * 
 */
@Embeddable
public class SuggestionwcPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int suggestingUserId;

	@Column(insertable=false, updatable=false)
	private int suggestedUserId;

	@Column(insertable=false, updatable=false)
	private int suggestedCardId;

	public SuggestionwcPK() {
	}
	public int getSuggestingUserId() {
		return this.suggestingUserId;
	}
	public void setSuggestingUserId(int suggestingUserId) {
		this.suggestingUserId = suggestingUserId;
	}
	public int getSuggestedUserId() {
		return this.suggestedUserId;
	}
	public void setSuggestedUserId(int suggestedUserId) {
		this.suggestedUserId = suggestedUserId;
	}
	public int getSuggestedCardId() {
		return this.suggestedCardId;
	}
	public void setSuggestedCardId(int suggestedCardId) {
		this.suggestedCardId = suggestedCardId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SuggestionwcPK)) {
			return false;
		}
		SuggestionwcPK castOther = (SuggestionwcPK)other;
		return 
			(this.suggestingUserId == castOther.suggestingUserId)
			&& (this.suggestedUserId == castOther.suggestedUserId)
			&& (this.suggestedCardId == castOther.suggestedCardId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.suggestingUserId;
		hash = hash * prime + this.suggestedUserId;
		hash = hash * prime + this.suggestedCardId;
		
		return hash;
	}
}