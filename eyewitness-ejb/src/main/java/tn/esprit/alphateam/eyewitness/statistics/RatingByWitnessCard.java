package tn.esprit.alphateam.eyewitness.statistics;

import java.io.Serializable;

public class RatingByWitnessCard implements Serializable {
	private static final long serialVersionUID = 7665676821022908929L;

	private int rating;
	private long number;

	public RatingByWitnessCard(int rating, long number) {
		super();
		this.rating = rating;
		this.number = number;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "RatingByWitnessCard [rating=" + rating + ", number=" + number + "]";
	}
	
}
