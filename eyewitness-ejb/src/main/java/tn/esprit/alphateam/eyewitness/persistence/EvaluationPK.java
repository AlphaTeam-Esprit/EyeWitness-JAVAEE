package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the evaluations database table.
 * 
 */
@Embeddable
public class EvaluationPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int witnessCardId;

	@Column(insertable=false, updatable=false)
	private int userId;

	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date evaluationDate;

	public EvaluationPK() {
	}
	public int getWitnessCardId() {
		return this.witnessCardId;
	}
	public void setWitnessCardId(int witnessCardId) {
		this.witnessCardId = witnessCardId;
	}
	public int getUserId() {
		return this.userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public java.util.Date getEvaluationDate() {
		return this.evaluationDate;
	}
	public void setEvaluationDate(java.util.Date evaluationDate) {
		this.evaluationDate = evaluationDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EvaluationPK)) {
			return false;
		}
		EvaluationPK castOther = (EvaluationPK)other;
		return 
			(this.witnessCardId == castOther.witnessCardId)
			&& (this.userId == castOther.userId)
			&& this.evaluationDate.equals(castOther.evaluationDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.witnessCardId;
		hash = hash * prime + this.userId;
		hash = hash * prime + this.evaluationDate.hashCode();
		
		return hash;
	}
}