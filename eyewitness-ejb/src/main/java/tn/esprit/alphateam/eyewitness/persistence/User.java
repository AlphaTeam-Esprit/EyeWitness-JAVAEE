package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name = "users")
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Lob
	private String address;

	@Lob
	private String avatar;

	@Temporal(TemporalType.TIMESTAMP)
	private Date birthDate;

	@Lob
	private String email;

	@Lob
	private String firstName;

	@Lob
	private String lastName;

	private float noteXP;

	@Lob
	private String password;

	@Lob
	private String phoneNumber;

	@Lob
	private String role;

	@Lob
	private String status;

	// bi-directional many-to-one association to Evaluation
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private List<Evaluation> evaluations;

	// bi-directional many-to-one association to Subscriptionswc
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private List<Subscriptionswc> subscriptionswcs;

	// bi-directional many-to-one association to Suggestionwc
	@OneToMany(mappedBy = "suggestingUser", fetch = FetchType.EAGER)
	private List<Suggestionwc> suggestingSuggestions;

	// bi-directional many-to-one association to Suggestionwc
	@OneToMany(mappedBy = "suggestedUser", fetch = FetchType.EAGER)
	private List<Suggestionwc> suggestedSuggestions;

	// uni-directional many-to-many association to User
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "followers", joinColumns = { @JoinColumn(name = "UserId") }, inverseJoinColumns = {
			@JoinColumn(name = "FollowerId") })
	private List<User> followers;

	// bi-directional many-to-one association to Witnesscard
	@OneToMany(mappedBy = "creator", fetch = FetchType.EAGER)
	private List<WitnessCard> witnessCardsCreated;

	// bi-directional many-to-one association to Witnesscard
	@OneToMany(mappedBy = "agent", fetch = FetchType.EAGER)
	private List<WitnessCard> witnessCardManaged;

	@OneToMany(mappedBy = "creator")
	private List<ForumTopic> forumTopics;

	@OneToMany(mappedBy = "author")
	private List<ForumMessage> forumMessages;

	@OneToMany(mappedBy = "userReporting")
	private List<ForumReport> forumReports;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private List<Notification> notificationMessages;

	@OneToMany(mappedBy = "user")
	private List<WitnessCardRating> witnessCardRatings;

	@ManyToMany
	private List<Competition> competitions;

	@ManyToMany
	private List<Event> events;
	
	private Date endDate;

	private int nbBadComments;

	public User() {
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}
	
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getNbBadComments() {
		return nbBadComments;
	}

	public void setNbBadComments(int nbBadComments) {
		this.nbBadComments = nbBadComments;
	}

	public List<WitnessCardRating> getWitnessCardRatings() {
		return witnessCardRatings;
	}

	public void setWitnessCardRatings(List<WitnessCardRating> witnessCardRatings) {
		this.witnessCardRatings = witnessCardRatings;
	}

	public List<Notification> getNotificationMessages() {
		return notificationMessages;
	}

	public void setNotificationMessages(List<Notification> notificationMessages) {
		this.notificationMessages = notificationMessages;
	}

	public List<ForumReport> getForumReports() {
		return forumReports;
	}

	public void setForumReports(List<ForumReport> forumReports) {
		this.forumReports = forumReports;
	}

	public List<ForumTopic> getForumTopics() {
		return forumTopics;
	}

	public void setForumTopics(List<ForumTopic> forumTopics) {
		this.forumTopics = forumTopics;
	}

	public List<ForumMessage> getForumMessages() {
		return forumMessages;
	}

	public void setForumMessages(List<ForumMessage> forumMessages) {
		this.forumMessages = forumMessages;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvatar() {
		return "/content/avatars/" + this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public float getNoteXP() {
		return this.noteXP;
	}

	public void setNoteXP(float noteXP) {
		this.noteXP = noteXP;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Evaluation> getEvaluations() {
		return this.evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public Evaluation addEvaluation(Evaluation evaluation) {
		getEvaluations().add(evaluation);
		evaluation.setUser(this);

		return evaluation;
	}

	public Evaluation removeEvaluation(Evaluation evaluation) {
		getEvaluations().remove(evaluation);
		evaluation.setUser(null);

		return evaluation;
	}

	public List<Subscriptionswc> getSubscriptionswcs() {
		return this.subscriptionswcs;
	}

	public void setSubscriptionswcs(List<Subscriptionswc> subscriptionswcs) {
		this.subscriptionswcs = subscriptionswcs;
	}

	public Subscriptionswc addSubscriptionswc(Subscriptionswc subscriptionswc) {
		getSubscriptionswcs().add(subscriptionswc);
		subscriptionswc.setUser(this);

		return subscriptionswc;
	}

	public Subscriptionswc removeSubscriptionswc(Subscriptionswc subscriptionswc) {
		getSubscriptionswcs().remove(subscriptionswc);
		subscriptionswc.setUser(null);

		return subscriptionswc;
	}

	public List<Suggestionwc> getSuggestingSuggestions() {
		return this.suggestingSuggestions;
	}

	public void setSuggestingSuggestions(List<Suggestionwc> suggestingSuggestions) {
		this.suggestingSuggestions = suggestingSuggestions;
	}

	public Suggestionwc addSuggestingSuggestion(Suggestionwc suggestingSuggestion) {
		getSuggestingSuggestions().add(suggestingSuggestion);
		suggestingSuggestion.setSuggestingUser(this);

		return suggestingSuggestion;
	}

	public Suggestionwc removeSuggestingSuggestion(Suggestionwc suggestingSuggestion) {
		getSuggestingSuggestions().remove(suggestingSuggestion);
		suggestingSuggestion.setSuggestingUser(null);

		return suggestingSuggestion;
	}

	public List<Suggestionwc> getSuggestedSuggestions() {
		return this.suggestedSuggestions;
	}

	public void setSuggestedSuggestions(List<Suggestionwc> suggestedSuggestions) {
		this.suggestedSuggestions = suggestedSuggestions;
	}

	public Suggestionwc addSuggestedSuggestion(Suggestionwc suggestedSuggestion) {
		getSuggestedSuggestions().add(suggestedSuggestion);
		suggestedSuggestion.setSuggestedUser(this);

		return suggestedSuggestion;
	}

	public Suggestionwc removeSuggestedSuggestion(Suggestionwc suggestedSuggestion) {
		getSuggestedSuggestions().remove(suggestedSuggestion);
		suggestedSuggestion.setSuggestedUser(null);

		return suggestedSuggestion;
	}

	public List<User> getFollowers() {
		return this.followers;
	}

	public void setFollowers(List<User> followers) {
		this.followers = followers;
	}

	public List<WitnessCard> getWitnessCardsCreated() {
		return this.witnessCardsCreated;
	}

	public void setWitnessCardsCreated(List<WitnessCard> witnessCardsCreated) {
		this.witnessCardsCreated = witnessCardsCreated;
	}

	public WitnessCard addWitnessCardsCreated(WitnessCard witnessCardsCreated) {
		getWitnessCardsCreated().add(witnessCardsCreated);
		witnessCardsCreated.setCreator(this);

		return witnessCardsCreated;
	}

	public WitnessCard removeWitnessCardsCreated(WitnessCard witnessCardsCreated) {
		getWitnessCardsCreated().remove(witnessCardsCreated);
		witnessCardsCreated.setCreator(null);

		return witnessCardsCreated;
	}

	public List<WitnessCard> getWitnessCardManaged() {
		return this.witnessCardManaged;
	}

	public void setWitnessCardManaged(List<WitnessCard> witnessCardManaged) {
		this.witnessCardManaged = witnessCardManaged;
	}

	public WitnessCard addWitnessCardManaged(WitnessCard witnessCardManaged) {
		getWitnessCardManaged().add(witnessCardManaged);
		witnessCardManaged.setAgent(this);

		return witnessCardManaged;
	}

	public WitnessCard removeWitnessCardManaged(WitnessCard witnessCardManaged) {
		getWitnessCardManaged().remove(witnessCardManaged);
		witnessCardManaged.setAgent(null);

		return witnessCardManaged;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

}