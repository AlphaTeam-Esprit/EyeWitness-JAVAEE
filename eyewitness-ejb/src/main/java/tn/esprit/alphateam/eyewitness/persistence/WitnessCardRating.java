package tn.esprit.alphateam.eyewitness.persistence;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class WitnessCardRating {

	@EmbeddedId
	private WitnessCardRatingId id;

	@ManyToOne
	@JoinColumn(name = "idUser", referencedColumnName = "id", updatable = false, insertable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "idWitnessCard", referencedColumnName = "id", updatable = false, insertable = false)
	private WitnessCard witnesscard;

	private int rating;
	
	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public WitnessCardRatingId getId() {
		return id;
	}

	public void setId(WitnessCardRatingId id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

}
