package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.alphateam.eyewitness.persistence.AgentRequest;
import tn.esprit.alphateam.eyewitness.persistence.Competition;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.UserReport;

@Local
public interface UserServicesLocal {

	User login(String email, String password);
	void register(User user);
	void sendEmailToUser(String textMessage, String subject, String userEmail);
	User findUserById(Integer id);
	public void sendEmailToUser(String textMessage, String subject, String userEmail, String base64Image);
	void banUser(Integer id, int durationDays);
	List<User> findBannableUsers();
	List<User> findBannedUsers();
	void unbanUser(Integer id);
	void assignAgent(AgentRequest agentRequest);
	void deleteAgentRequest(Integer witnessCardId);
	List<AgentRequest>findAgentRequest();
	List<AgentRequest>findAgentRequestActive();
	void deleteAgentRequestBywitnessCardIdAndAgentId(Integer witnessCardId,Integer agentId);
	void demoteAgent(AgentRequest agentRequest);
	Long getCommentCount(int id);
	List<User> findAllUsers();
	void participateCompetition(User user, Competition competition);
	List<User>findAgents();
	int getReportingByIdUserReported(int idUserReported);
	void reportUserById(UserReport userReport);
	void BanBadUserById(int idUser);
	void incrementBadWords(int idbadUser);
}
