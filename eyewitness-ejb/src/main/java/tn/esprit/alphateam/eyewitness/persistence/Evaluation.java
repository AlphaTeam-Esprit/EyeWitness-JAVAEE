package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the evaluations database table.
 * 
 */
@Entity
@Table(name="evaluations")
@NamedQuery(name="Evaluation.findAll", query="SELECT e FROM Evaluation e")
public class Evaluation implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EvaluationPK id;

	@Lob
	private String comment;

	@Lob
	private String pictures;

	private int rating;

	@Lob
	private String video;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="UserId")
	private User user;

	//bi-directional many-to-one association to Witnesscard
	@ManyToOne
	@JoinColumn(name="WitnessCardId")
	private WitnessCard witnesscard;

	public Evaluation() {
	}

	public EvaluationPK getId() {
		return this.id;
	}

	public void setId(EvaluationPK id) {
		this.id = id;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPictures() {
		return this.pictures;
	}

	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

	public int getRating() {
		return this.rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getVideo() {
		return this.video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public WitnessCard getWitnesscard() {
		return this.witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

}