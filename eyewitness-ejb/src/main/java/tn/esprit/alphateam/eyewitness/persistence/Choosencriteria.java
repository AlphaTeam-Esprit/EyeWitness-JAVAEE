package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the choosencriterias database table.
 * 
 */
@Entity
@Table(name="choosencriterias")
@NamedQuery(name="Choosencriteria.findAll", query="SELECT c FROM Choosencriteria c")
public class Choosencriteria implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ChoosencriteriaPK id;

	private int vote;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="CategoryId")
	private Category category;

	//bi-directional many-to-one association to Criteria
	@ManyToOne
	@JoinColumn(name="CriteriasId")
	private Criteria criteria;

	public Choosencriteria() {
	}

	public ChoosencriteriaPK getId() {
		return this.id;
	}

	public void setId(ChoosencriteriaPK id) {
		this.id = id;
	}

	public int getVote() {
		return this.vote;
	}

	public void setVote(int vote) {
		this.vote = vote;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Criteria getCriteria() {
		return this.criteria;
	}

	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}

}