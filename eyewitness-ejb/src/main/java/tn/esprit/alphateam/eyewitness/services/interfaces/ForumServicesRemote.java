package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.ForumMessage;
import tn.esprit.alphateam.eyewitness.persistence.ForumReport;
import tn.esprit.alphateam.eyewitness.persistence.ForumSection;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic;
import tn.esprit.alphateam.eyewitness.persistence.User;

@Remote
public interface ForumServicesRemote {
	List<ForumSection> findAllSections();

	List<ForumTopic> findTopicsBySubjectId(int subjectId);

	List<ForumSubject> findSubjectsBySectionId(int sectionId);

	List<ForumTopic> findHotTopics();

	void saveOrUpdateSubject(ForumSubject subject);

	ForumSection findSectionById(int idSection);

	List<User> findTopPosters();

	List<ForumTopic> findMostNotoriousTopics();

	void saveOrUpdateTopic(ForumTopic topic);

	void deleteTopic(ForumTopic topic);

	ForumTopic findTopicById(int topicId);

	List<ForumMessage> findMessagesByTopicId(int topicId);

	void saveOrUpdateMessage(ForumMessage message);

	void reportMessage(ForumReport report);

	void saveOrUpdateSection(ForumSection section);

	List<ForumReport> findAllReports();

	int findNumberOfMessagesBySubjectId(int subjectId);

	ForumSubject findSubjectById(int subjectId);

	ForumTopic findLatestTopicBySubjectId(int subjectId);

	List<ForumMessage> findMessagesByTopicIdOrderByDate(int topicId);
}
