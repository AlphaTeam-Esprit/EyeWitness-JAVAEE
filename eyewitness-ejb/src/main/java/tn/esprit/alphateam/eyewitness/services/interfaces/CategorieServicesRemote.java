package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.Category;


@Remote
public interface CategorieServicesRemote {
	List<Category> findCategoriesByStatus();
	void validateCategorie(Integer id);
	void deleteCategorie(Category category);
}
