package tn.esprit.alphateam.eyewitness.services.impl;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.AgentRequest;
import tn.esprit.alphateam.eyewitness.persistence.Competition;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.UserReport;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesRemote;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;

/**
 * Session Bean implementation class ProductServices
 */
@Stateless
public class UserServices implements UserServicesRemote, UserServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	@Resource(name = "java:jboss/mail/eyewitness-mail")
	private Session session;
	
	@EJB
	WitnessCardServicesLocal witnessCardsServicesLocal;

	public UserServices() {
	}

	@Override
	public User login(String email, String password) {
		User user = null;

		Query query = entityManager.createQuery("SELECT u FROM User u WHERE u.email=:email AND u.password=:password");
		query.setParameter("email", email);
		query.setParameter("password", password);

		try {
			user = (User) query.getSingleResult();
		} catch (Exception e) {
			System.err.println("Error when trying to login user !");
		}

		return user;
	}

	@Override
	public void register(User user) {
		entityManager.merge(user);
	}

	@Override
	public User findUserById(Integer id) {
		return entityManager.find(User.class, id);
	}

	@Override
	public Long getCommentCount(int id){
		Query query =  entityManager.createQuery("SELECT COUNT(e) FROM Evaluation e WHERE e.id.userId = :id"); 
		
		query.setParameter("id", id);
		
		return (Long) query.getSingleResult();
	}

	@Override
	public List<User> findAllUsers() {
		return entityManager.createQuery("SELECT u FROM User u").getResultList();
	}

	@Override
	public void participateCompetition(User user, Competition competition) {
		user = entityManager.find(User.class, user.getId());
		user.getCompetitions().add(competition);
		entityManager.merge(user);
	}

	@Override
	public void sendEmailToUser(String textMessage, String subject, String userEmail) {

		try {

			MimeMessage message = new MimeMessage(session);
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(userEmail, "Eyewitness support"));
			message.setSubject(subject);
			// message.setText(textMessage);
			message.setContent(textMessage, "text/html");
			Transport.send(message);

		} catch (MessagingException | UnsupportedEncodingException e) {
			e.printStackTrace();
			System.err.println("Cannot send email !");
		}

	}

	@Override
	public void sendEmailToUser(String textMessage, String subject, String userEmail, String base64Image) {

		try {

			Message message = new MimeMessage(session);

			message.setRecipient(Message.RecipientType.TO, new InternetAddress(userEmail));

			message.setSubject(subject);

			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setText(textMessage);

			Multipart multipart = new MimeMultipart();

			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			byte[] imgBytes = Base64.getDecoder().decode(base64Image.replace("data:image/png;base64,", ""));
			DataSource source = new ByteArrayDataSource(imgBytes, "image/*");
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("stats.png");
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);

			Transport.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
			System.err.println("Cannot send email !");
		}

	}

	@Override
	public void banUser(Integer id, int durationDays) {

		Calendar c = Calendar.getInstance();
		String jpql = "UPDATE User u set u.status=:param , u.endDate=:param1 where u.id=:param2";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "BANNED");

		c.setTime(new Date());
		c.add(Calendar.DAY_OF_MONTH, durationDays);

		query.setParameter("param1", c.getTime());
		query.setParameter("param2", id);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findBannableUsers() {

		String jpql = "SELECT u FROM User u WHERE u.role!=:param AND u.status=:param1 AND u.nbBadComments>=:param2";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "ADMIN");
		query.setParameter("param1", "ACTIVE");
		query.setParameter("param2", 5);
		return query.getResultList();
	}

	@Override
	public void unbanUser(Integer id) {

		String jpql = "UPDATE User u set u.status=:param ,u.endDate=:param1 ,u.nbBadComments=:param2 where u.id=:param3 ";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "ACTIVE");
		query.setParameter("param1", null);
		query.setParameter("param2", 0);
		query.setParameter("param3", id);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAgents() {

		String jpql = "SELECT a FROM User a WHERE a.role=:param ";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "AGENT");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findBannedUsers() {
		String jpql = "SELECT u FROM User u WHERE u.role!=:param AND u.status=:param1";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "ADMIN");
		query.setParameter("param1", "BANNED");

		return query.getResultList();

	}

	@Override
	public void assignAgent(AgentRequest agentRequest) {

		User agentFound = findUserById(agentRequest.getAgent().getId());
		agentFound.setRole("AGENT");
		WitnessCard witnesscard = witnessCardsServicesLocal.findWitnessCardById(agentRequest.getWitnesscard().getId());
		witnesscard.setAgent(agentFound);
		agentRequest.setStatus("VALID");
		entityManager.merge(agentFound);
		entityManager.merge(witnesscard);
		entityManager.merge(agentRequest);
		deleteAgentRequest(agentRequest.getWitnesscard().getId());

	}

	@Override
	public void deleteAgentRequest(Integer witnessCardId) {
		String jpql = "DELETE  FROM AgentRequest ag WHERE ag.status=:param AND ag.witnesscard.id=:param1";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "STANDBY");
		query.setParameter("param1", witnessCardId);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentRequest> findAgentRequest() {

		String jpql = "SELECT ag FROM AgentRequest ag WHERE ag.status=:param";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "STANDBY");

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentRequest> findAgentRequestActive() {
		String jpql = "SELECT ag FROM AgentRequest ag WHERE ag.status=:param";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "VALID");

		return query.getResultList();
	}

	@Override
	public void demoteAgent(AgentRequest agentRequest) {
		User agent = agentRequest.getAgent();
		deleteAgentRequestBywitnessCardIdAndAgentId(agentRequest.getWitnesscard().getId(),
				agentRequest.getAgent().getId());

		WitnessCard witnesscardFound = witnessCardsServicesLocal
				.findWitnessCardById(agentRequest.getWitnesscard().getId());
		witnesscardFound.setAgent(null);
		entityManager.merge(witnesscardFound);

		int numberOfWitnessCards = agent.getWitnessCardManaged().size();
		numberOfWitnessCards--;
		if (numberOfWitnessCards == 0) {
			agent.setRole("USER");
			entityManager.merge(agent);
		}

	}

	@Override
	public void deleteAgentRequestBywitnessCardIdAndAgentId(Integer witnessCardId, Integer agentId) {
		String jpql = "DELETE  FROM AgentRequest ag WHERE ag.status=:param AND ag.witnesscard.id=:param1 AND ag.agent.id=:param2";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param", "VALID");
		query.setParameter("param1", witnessCardId);
		query.setParameter("param2", agentId);
		query.executeUpdate();

	}
	
	@Override
	public void reportUserById(UserReport userReport) {

		entityManager.merge(userReport);
		int i = getReportingByIdUserReported(userReport.getIdReported());
		incrementBadWords(userReport.getIdReported());

		if (i >= 5) {
			BanBadUserById(userReport.getIdReported());
			User userReported = entityManager.find(User.class, userReport.getIdReported());
			String textmsg = " you are banned from our plateform for misconducting ( bad words comments)";
			String subject = "Ban From Eyewitness Notifacation ";
			String Emailto = userReported.getEmail();

			sendEmailToUser(textmsg, subject, Emailto);

			// ici lapi Mailer va intervenir
		}

	}

	@Override
	public int getReportingByIdUserReported(int idUserReported) {

		String jpql = "SELECT  w FROM UserReport w  where w.idReported=:param1";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("param1", idUserReported);

		return query.getResultList().size();
	}

	@Override
	public void BanBadUserById(int idUser) {
		User user = entityManager.find(User.class, idUser);
		user.setStatus("Blocked");
		entityManager.merge(user);

	}
	
	@Override
	public void incrementBadWords(int idbadUser) {
		User user = entityManager.find(User.class, idbadUser);
		user.setNbBadComments((user.getNbBadComments()) + 1);
		entityManager.merge(user);

	}


}
