package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the witnesscards database table.
 * 
 */
@Entity
@Table(name = "witnesscards")
@NamedQuery(name = "WitnessCard.findAll", query = "SELECT w FROM WitnessCard w")
public class WitnessCard implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Lob
	private String address;

	@Lob
	private String ambience;

	@Lob
	private String description;

	private float latitude;

	private float longitude;

	@Lob
	private String meansOfPayment;

	@Lob
	private String name;

	private int nbuser;

	private float note;

	@Lob
	private String offeredServices;

	@Lob
	private String picture;

	@Lob
	private String status;

	// bi-directional many-to-one association to Competition
	@OneToMany(mappedBy = "witnesscard", fetch = FetchType.EAGER)
	private List<Competition> competitions;

	// bi-directional many-to-one association to Evaluation
	@OneToMany(mappedBy = "witnesscard", fetch = FetchType.EAGER)
	private List<Evaluation> evaluations;

	// bi-directional many-to-one association to Event
	@OneToMany(mappedBy = "witnesscard", fetch = FetchType.EAGER)
	private List<Event> events;

	// bi-directional many-to-one association to Subscriptionswc
	@OneToMany(mappedBy = "witnesscard", fetch = FetchType.EAGER)
	private List<Subscriptionswc> subscriptionswcs;

	// bi-directional many-to-one association to Suggestionwc
	@OneToMany(mappedBy = "witnesscard", fetch = FetchType.EAGER)
	private List<Suggestionwc> suggestionwcs;

	// bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name = "CategoryId")
	private Category category;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "creatorId")
	private User creator;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "agentId")
	private User agent;

	@OneToMany(mappedBy = "witnesscard", fetch = FetchType.EAGER)
	private List<WitnessCardStatistic> statistics;

	@OneToMany(mappedBy = "witnesscard")
	private List<WitnessCardRating> witnessCardRatings;

	@OneToMany(mappedBy = "aboutWitnessCard")
	private List<ForumTopic> forumTopics;

	private boolean isPayed;

	public WitnessCard() {
	}

	public boolean getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(boolean isPayed) {
		this.isPayed = isPayed;
	}

	public List<ForumTopic> getForumTopics() {
		return forumTopics;
	}

	public void setForumTopics(List<ForumTopic> forumTopics) {
		this.forumTopics = forumTopics;
	}

	public List<WitnessCardRating> getWitnessCardRatings() {
		return witnessCardRatings;
	}

	public void setWitnessCardRatings(List<WitnessCardRating> witnessCardRatings) {
		this.witnessCardRatings = witnessCardRatings;
	}

	public List<WitnessCardStatistic> getStatistics() {
		return statistics;
	}

	public void setStatistics(List<WitnessCardStatistic> statistics) {
		this.statistics = statistics;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAmbience() {
		return this.ambience;
	}

	public void setAmbience(String ambience) {
		this.ambience = ambience;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getLatitude() {
		return this.latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return this.longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getMeansOfPayment() {
		return this.meansOfPayment;
	}

	public void setMeansOfPayment(String meansOfPayment) {
		this.meansOfPayment = meansOfPayment;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNbuser() {
		return this.nbuser;
	}

	public void setNbuser(int nbuser) {
		this.nbuser = nbuser;
	}

	public float getNote() {
		return this.note;
	}

	public void setNote(float note) {
		this.note = note;
	}

	public String getOfferedServices() {
		return this.offeredServices;
	}

	public void setOfferedServices(String offeredServices) {
		this.offeredServices = offeredServices;
	}

	public String getPicture() {
		return "/content/" + this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Competition> getCompetitions() {
		return this.competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}

	public Competition addCompetition(Competition competition) {
		getCompetitions().add(competition);
		competition.setWitnesscard(this);

		return competition;
	}

	public Competition removeCompetition(Competition competition) {
		getCompetitions().remove(competition);
		competition.setWitnesscard(null);

		return competition;
	}

	public List<Evaluation> getEvaluations() {
		return this.evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public Evaluation addEvaluation(Evaluation evaluation) {
		getEvaluations().add(evaluation);
		evaluation.setWitnesscard(this);

		return evaluation;
	}

	public Evaluation removeEvaluation(Evaluation evaluation) {
		getEvaluations().remove(evaluation);
		evaluation.setWitnesscard(null);

		return evaluation;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setWitnesscard(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setWitnesscard(null);

		return event;
	}

	public List<Subscriptionswc> getSubscriptionswcs() {
		return this.subscriptionswcs;
	}

	public void setSubscriptionswcs(List<Subscriptionswc> subscriptionswcs) {
		this.subscriptionswcs = subscriptionswcs;
	}

	public Subscriptionswc addSubscriptionswc(Subscriptionswc subscriptionswc) {
		getSubscriptionswcs().add(subscriptionswc);
		subscriptionswc.setWitnesscard(this);

		return subscriptionswc;
	}

	public Subscriptionswc removeSubscriptionswc(Subscriptionswc subscriptionswc) {
		getSubscriptionswcs().remove(subscriptionswc);
		subscriptionswc.setWitnesscard(null);

		return subscriptionswc;
	}

	public List<Suggestionwc> getSuggestionwcs() {
		return this.suggestionwcs;
	}

	public void setSuggestionwcs(List<Suggestionwc> suggestionwcs) {
		this.suggestionwcs = suggestionwcs;
	}

	public Suggestionwc addSuggestionwc(Suggestionwc suggestionwc) {
		getSuggestionwcs().add(suggestionwc);
		suggestionwc.setWitnesscard(this);

		return suggestionwc;
	}

	public Suggestionwc removeSuggestionwc(Suggestionwc suggestionwc) {
		getSuggestionwcs().remove(suggestionwc);
		suggestionwc.setWitnesscard(null);

		return suggestionwc;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getCreator() {
		return this.creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public User getAgent() {
		return this.agent;
	}

	public void setAgent(User agent) {
		this.agent = agent;
	}

}