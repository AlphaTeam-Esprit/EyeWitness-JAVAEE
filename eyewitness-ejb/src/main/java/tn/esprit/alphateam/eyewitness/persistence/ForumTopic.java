package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: ForumTopic
 *
 */
@Entity
public class ForumTopic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String title;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Enumerated(EnumType.STRING)
	private ForumTopicState state;
	private boolean isPinned;

	@ManyToOne
	private ForumSubject forumSubject;

	@ManyToOne
	private User creator;

	@OneToMany(mappedBy = "forumTopic", fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
	private List<ForumMessage> forumMessages;

	@ManyToOne
	private WitnessCard aboutWitnessCard;

	public ForumTopic() {
		super();
	}

	public WitnessCard getAboutWitnessCard() {
		return aboutWitnessCard;
	}

	public void setAboutWitnessCard(WitnessCard aboutWitnessCard) {
		this.aboutWitnessCard = aboutWitnessCard;
	}

	public List<ForumMessage> getForumMessages() {
		return forumMessages;
	}

	public void setForumMessages(List<ForumMessage> forumMessages) {
		this.forumMessages = forumMessages;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public ForumSubject getForumSubject() {
		return forumSubject;
	}

	public void setForumSubject(ForumSubject forumSubject) {
		this.forumSubject = forumSubject;
	}

	public void setPinned(boolean isPinned) {
		this.isPinned = isPinned;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public ForumTopicState getState() {
		return this.state;
	}

	public void setState(ForumTopicState state) {
		this.state = state;
	}

	public boolean getIsPinned() {
		return this.isPinned;
	}

	public void setIsPinned(boolean isPinned) {
		this.isPinned = isPinned;
	}

	public enum ForumTopicState {
		LOCKED, ONGOING, SOLVED
	}

}
