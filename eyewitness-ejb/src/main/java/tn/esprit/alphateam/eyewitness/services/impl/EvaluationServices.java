package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.alphateam.eyewitness.persistence.Evaluation;
import tn.esprit.alphateam.eyewitness.services.interfaces.EvaluationServicesLocal;

@Stateless
public class EvaluationServices implements EvaluationServicesLocal{
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Evaluation> findAll() {
		return entityManager.createQuery("SELECT e FROM Evaluation e").getResultList();
	} 
	
}
