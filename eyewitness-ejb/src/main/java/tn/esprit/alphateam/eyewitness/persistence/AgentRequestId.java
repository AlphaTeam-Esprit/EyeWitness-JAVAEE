package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: AgentRequestId
 *
 */
@Embeddable

public class AgentRequestId implements Serializable {

	
	private Integer witnessCardId;
	private Integer agentId;
	private static final long serialVersionUID = 1L;

	public AgentRequestId() {
		super();
	}   
	public Integer getWitnessCardId() {
		return this.witnessCardId;
	}

	public void setWitnessCardId(Integer witnessCardId) {
		this.witnessCardId = witnessCardId;
	}   
	public Integer getAgentId() {
		return this.agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	
	public AgentRequestId(Integer witnessCardId, Integer agentId) {
		super();
		this.witnessCardId = witnessCardId;
		this.agentId = agentId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentId == null) ? 0 : agentId.hashCode());
		result = prime * result + ((witnessCardId == null) ? 0 : witnessCardId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentRequestId other = (AgentRequestId) obj;
		if (agentId == null) {
			if (other.agentId != null)
				return false;
		} else if (!agentId.equals(other.agentId))
			return false;
		if (witnessCardId == null) {
			if (other.witnessCardId != null)
				return false;
		} else if (!witnessCardId.equals(other.witnessCardId))
			return false;
		return true;
	}

	
	
	
   
}
