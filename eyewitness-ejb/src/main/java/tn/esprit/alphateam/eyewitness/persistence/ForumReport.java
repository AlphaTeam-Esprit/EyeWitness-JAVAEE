package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 * Entity implementation class for Entity: ForumReport
 *
 */
@Entity
public class ForumReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Lob
	private String reason;
	
	@ManyToOne
	private ForumMessage forumMessage;
	
	@ManyToOne
	private User userReporting;

	public ForumReport() {
		super();
	}
	
	public User getUserReporting() {
		return userReporting;
	}

	public void setUserReporting(User userReporting) {
		this.userReporting = userReporting;
	}

	public ForumMessage getForumMessage() {
		return forumMessage;
	}

	public void setForumMessage(ForumMessage forumMessage) {
		this.forumMessage = forumMessage;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
