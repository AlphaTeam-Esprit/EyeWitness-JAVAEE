package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the choosencriterias database table.
 * 
 */
@Embeddable
public class ChoosencriteriaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int categoryId;

	@Column(insertable=false, updatable=false)
	private int criteriasId;

	public ChoosencriteriaPK() {
	}
	public int getCategoryId() {
		return this.categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getCriteriasId() {
		return this.criteriasId;
	}
	public void setCriteriasId(int criteriasId) {
		this.criteriasId = criteriasId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ChoosencriteriaPK)) {
			return false;
		}
		ChoosencriteriaPK castOther = (ChoosencriteriaPK)other;
		return 
			(this.categoryId == castOther.categoryId)
			&& (this.criteriasId == castOther.criteriasId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.categoryId;
		hash = hash * prime + this.criteriasId;
		
		return hash;
	}
}