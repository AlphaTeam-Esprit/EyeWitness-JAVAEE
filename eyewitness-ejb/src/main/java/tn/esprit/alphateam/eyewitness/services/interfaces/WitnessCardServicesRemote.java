package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.Comments;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.statistics.NumberOfSubscribersByDate;
import tn.esprit.alphateam.eyewitness.statistics.NumberOfVisitorsByDate;
import tn.esprit.alphateam.eyewitness.statistics.RatingByWitnessCard;

@Remote
public interface WitnessCardServicesRemote {

	void saveOrUpdateWitnessCard(WitnessCard c);

	List<NumberOfSubscribersByDate> findNumberOfSubscribersByDateByWitnessCardId(Integer cardId);

	List<NumberOfVisitorsByDate> findNumberOfVisitorByDateByWitnessCardId(Integer cardId);

	List<RatingByWitnessCard> findNumberOfRatingsByWitnessCardId(Integer cardId);

	public List<WitnessCard> listWitnessCards();

	public Long getCommentCount(int id);

	public void add(WitnessCard wc);

	public WitnessCard findWitnessById(Integer idwc);

	List<WitnessCard> findNoValidWitnessCard();

	WitnessCard findWitnessCardById(Integer id);

	void validateWitnessCard(int id);

	List<WitnessCard> findWitnessCardsByAgent(Integer AgentId);

	void DeleteWitnessCard(WitnessCard WitnessCard);

	List<WitnessCard> listWitnessCardsByCurrentAgent(int idAgent);

	WitnessCard findWitnesscardById(Integer id);

	List<WitnessCard> getAllWitnessCardsByname(String name);

	List<Comments> getAllCommentsByWitnessCardId(int idWitnessCard);

	List<WitnessCard> getAllWitnessCard();

	WitnessCard getWitnessCardByid(int idwitnessCard);

}
