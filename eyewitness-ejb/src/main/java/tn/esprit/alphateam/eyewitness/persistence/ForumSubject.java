package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ForumSubject implements Serializable {
	private static final long serialVersionUID = 4360416175414451809L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String title;
	private String description;
	private Boolean hasNew;

	@ManyToOne
	private ForumSection forumSection;

	@OneToMany(mappedBy = "forumSubject", fetch=FetchType.EAGER)
	private List<ForumTopic> forumTopics;

	public ForumSubject() {
		super();
	}
	
	public Boolean getHasNew() {
		return hasNew;
	}

	public void setHasNew(Boolean hasNew) {
		this.hasNew = hasNew;
	}

	public List<ForumTopic> getForumTopics() {
		return forumTopics;
	}

	public void setForumTopics(List<ForumTopic> forumTopics) {
		this.forumTopics = forumTopics;
	}

	public ForumSection getForumSection() {
		return forumSection;
	}

	public void setForumSection(ForumSection forumSection) {
		this.forumSection = forumSection;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
