package tn.esprit.alphateam.eyewitness.statistics;

import java.io.Serializable;
import java.util.Date;

public class NumberOfSubscribersByDate implements Serializable {
	private static final long serialVersionUID = -6231844421482819328L;
	
	private Date date;
	private long nbSubscribers;

	public NumberOfSubscribersByDate(Date date, long nbSubscribers) {
		super();
		this.date = date;
		this.nbSubscribers = nbSubscribers;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getNbSubscribers() {
		return nbSubscribers;
	}

	public void setNbSubscribers(long nbSubscribers) {
		this.nbSubscribers = nbSubscribers;
	}

	@Override
	public String toString() {
		return "NumberOfSubscribersByDate [date=" + date + ", nbSubscribers=" + nbSubscribers + "]";
	}

}
