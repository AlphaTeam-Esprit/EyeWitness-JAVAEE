package tn.esprit.alphateam.eyewitness.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the subscriptionswcs database table.
 * 
 */
@Embeddable
public class SubscriptionswcPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int witnessCardId;

	@Column(insertable=false, updatable=false)
	private int userId;

	public SubscriptionswcPK() {
	}
	public int getWitnessCardId() {
		return this.witnessCardId;
	}
	public void setWitnessCardId(int witnessCardId) {
		this.witnessCardId = witnessCardId;
	}
	public int getUserId() {
		return this.userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SubscriptionswcPK)) {
			return false;
		}
		SubscriptionswcPK castOther = (SubscriptionswcPK)other;
		return 
			(this.witnessCardId == castOther.witnessCardId)
			&& (this.userId == castOther.userId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.witnessCardId;
		hash = hash * prime + this.userId;
		
		return hash;
	}
}