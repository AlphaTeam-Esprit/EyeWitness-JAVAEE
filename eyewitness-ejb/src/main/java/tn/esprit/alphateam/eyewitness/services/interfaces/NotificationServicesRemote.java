package tn.esprit.alphateam.eyewitness.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.alphateam.eyewitness.persistence.Notification;

@Remote
public interface NotificationServicesRemote {
	void saveNotification(Notification n);
	List<Notification> findFiveLastNotificationsByUserId(Integer userId);
}
