package tn.esprit.alphateam.eyewitness.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.alphateam.eyewitness.persistence.ForumMessage;
import tn.esprit.alphateam.eyewitness.persistence.ForumReport;
import tn.esprit.alphateam.eyewitness.persistence.ForumSection;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesRemote;

/**
 * Session Bean implementation class ForumServices
 */
@Stateless
public class ForumServices implements ForumServicesRemote, ForumServicesLocal {

	@PersistenceContext
	private EntityManager entityManager;

	public ForumServices() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumSection> findAllSections() {
		Query query = entityManager.createQuery("SELECT s FROM ForumSection s");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumTopic> findTopicsBySubjectId(int subjectId) {
		Query query = entityManager.createQuery("SELECT t FROM ForumTopic t WHERE t.forumSubject.id=:id");
		query.setParameter("id", subjectId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumSubject> findSubjectsBySectionId(int sectionId) {
		Query query = entityManager.createQuery("SELECT s FROM ForumSubject s WHERE s.forumSection.id=:id");
		query.setParameter("id", sectionId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumTopic> findHotTopics() {
		Query query = entityManager.createQuery("SELECT t FROM ForumTopic t WHERE t.forumMessages.size != 0 ORDER BY t.forumMessages.size");
		query.setMaxResults(3);
		return query.getResultList();
	}

	@Override
	public List<User> findTopPosters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ForumTopic> findMostNotoriousTopics() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdateTopic(ForumTopic topic) {
		entityManager.merge(topic);
	}

	@Override
	public void deleteTopic(ForumTopic topic) {
		entityManager.remove(entityManager.merge(topic));
	}

	@Override
	public ForumTopic findTopicById(int topicId) {
		return entityManager.find(ForumTopic.class, topicId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumMessage> findMessagesByTopicId(int topicId) {
		Query query = entityManager.createQuery("SELECT m FROM ForumMessage m WHERE m.forumTopic.id=:id");
		query.setParameter("id", topicId);
		return query.getResultList();
	}

	@Override
	public void saveOrUpdateMessage(ForumMessage message) {
		entityManager.merge(message);
	}

	@Override
	public void reportMessage(ForumReport report) {
		entityManager.merge(report);
		// Notify admin for incoming report
		// Notify user for incoming report
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumReport> findAllReports() {
		Query query = entityManager.createQuery("SELECT r FROM ForumReport r");
		return query.getResultList();
	}

	@Override
	public void saveOrUpdateSection(ForumSection section) {
		entityManager.merge(section);
	}
	
	@Override
	public int findNumberOfMessagesBySubjectId(int subjectId){
		
		ForumSubject foundSubject = findSubjectById(subjectId);
		int sum = 0;
		for(ForumTopic t : foundSubject.getForumTopics()) {
			sum += t.getForumMessages().size();
		}
		
		return sum;
		
	}
	
	@Override
	public ForumSubject findSubjectById(int subjectId) {
		return entityManager.find(ForumSubject.class, subjectId);
	}

	@Override
	public ForumTopic findLatestTopicBySubjectId(int subjectId) {
		
		Query query = entityManager.createQuery("SELECT t FROM ForumTopic t WHERE t.forumSubject.id=:id ORDER BY t.creationDate DESC");
		query.setParameter("id", subjectId);
		query.setMaxResults(1);
		return (ForumTopic) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumMessage> findMessagesByTopicIdOrderByDate(int topicId) {
		Query query = entityManager.createQuery("SELECT m FROM ForumMessage m WHERE m.forumTopic.id=:id ORDER BY m.postDate");
		query.setParameter("id", topicId);
		return query.getResultList();
	}

	@Override
	public void saveOrUpdateSubject(ForumSubject subject) {
		entityManager.merge(subject);
	}

	@Override
	public ForumSection findSectionById(int idSection) {
		return entityManager.find(ForumSection.class, idSection);
	}

}
