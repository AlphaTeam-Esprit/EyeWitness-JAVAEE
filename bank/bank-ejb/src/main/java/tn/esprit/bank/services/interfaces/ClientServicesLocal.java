package tn.esprit.bank.services.interfaces;

import javax.ejb.Local;
import javax.naming.NamingException;

import tn.esprit.bank.persistence.Client;

@Local
public interface ClientServicesLocal {
	void saveOrUpdateClient(Client client);

	void deleteClient(Client client);

	Client findClientByCode(String code);

	Boolean makePayment(String code,Integer ccv, String WitnessCardName) throws NamingException;

}
