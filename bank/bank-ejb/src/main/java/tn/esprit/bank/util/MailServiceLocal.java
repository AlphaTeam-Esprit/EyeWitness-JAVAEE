package tn.esprit.bank.util;

import javax.ejb.Local;
import javax.naming.NamingException;

@Local
public interface MailServiceLocal {
	void send(String addresses, String topic, String textMessage)throws NamingException ;
}
