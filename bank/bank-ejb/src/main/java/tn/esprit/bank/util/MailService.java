package tn.esprit.bank.util;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Session Bean implementation class MailService
 */
@Stateless
@LocalBean
public class MailService implements MailServiceRemote, MailServiceLocal {
	
	/**
     * Default constructor. 
     */
    public MailService() {
        // TODO Auto-generated constructor stub
    }
    
  
	@Override
	public void send(String addresses, String topic, String textMessage) throws NamingException {
		
		try {
			InitialContext ic = new InitialContext();
			Session session = (Session) ic.lookup("java:jboss/mail/eyewitness-mail");
			Message message=new MimeMessage(session);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(addresses));
			message.setSubject(topic);
			message.setText(textMessage);
			
			Transport.send(message);
		} catch (MessagingException e) {
			System.out.println(e);
		}
	}

}
