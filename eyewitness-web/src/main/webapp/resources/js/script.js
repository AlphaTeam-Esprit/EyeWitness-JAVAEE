var menuIsOpen = false;
$(window).ready(function () {

    // Initializers
	
    $('.tooltipped').material_tooltip({ delay: 50 });
    $('select').material_select();
    $('.modal').modal({
        dismissible: true,
        opacity: 0,
        in_duration: 300,
        out_duration: 200,
        starting_top: '25%',
        ending_top: '25%',
        ready: function (modal, trigger) {
            //console.log(modal, trigger);
            $('.modal-overlay').attr('style', 'z-index: 1;display: block; opacity: 0.5');
        },
    }
  )


    // Invisible navbar on home page
    if (window.location.pathname == "/" || window.location.pathname == "/Home" || window.location.pathname == "/Home/Index") {
        $('header > div > nav').css('background-color', 'transparent');
        $('header > div > nav').css('box-shadow', 'none');
    }

    // Fast search
    /*
    $('#fast-search-results').hide();

    $('#fast-search-input').focusin(function (e) {
        $('#fast-search-results').slideDown();
    });

    $('#fast-search-input').focusout(function (e) {
        $('#fast-search-results').slideUp();
    });*/
    
    // Fix search bar
    if (!(window.location.pathname == "/" || window.location.pathname == "/Home" || window.location.pathname == "/Home/Index")) {
        $('header nav .nav-wrapper > ul.left input').css({
            'bottom': '14px',
            'color': '#808080'
        })

        $('#fast-search-results').css({
            'z-index': '998'
        })
    }

    // Fast search input 
    $('#fast-search-input').keyup(function (e) {
        var value = $(this).val();
        
        if (value != '') {
            // Look for witness Cards By name
            
            $.ajax({
                url: '/api/SearchApi/FindWitnessCardByName?searchString=' + value,
                success: function (res) {
                    // Empty old results
                    $('.witness-card-results').hide();
                    $('.witness-card-results > div.results').html('');
                    if (res.length == 0) return;
                    $('.witness-card-results').show();
                    // Append new results
                    for (i = 0; i < res.length; i++) {
                        card = res[i];
                        chip = createChip(card.name, '/WitnessCard/Index/' + card.id, card.image)
                        oldHtml = $('.witness-card-results > div.results').html();
                        $('.witness-card-results > div.results').html(oldHtml + chip);
                    }
                }
            })

            // Look for Categories By name
            $.ajax({
                url: '/api/SearchApi/FindCategoryByName?searchString=' + value,
                success: function (res) {
                    // Empty old results
                    $('.category-results').hide();
                    $('.category-results > div.results').html('');
                    if (res.length == 0) return;
                    $('.category-results').show();
                    // Append new results
                    for (i = 0; i < res.length; i++) {
                        cat = res[i];
                        chip = createChip(cat.name, '/WitnessCard/TopTen/' + cat.id, cat.image)
                        oldHtml = $('.category-results > div.results').html();
                        $('.category-results > div.results').html(oldHtml + chip);
                    }
                }
            })

            // Look for Users By name
            $.ajax({
                url: '/api/SearchApi/FindUserByName?searchString=' + value,
                success: function (res) {
                    // Empty old results
                    $('.user-results').hide();
                    $('.user-results > div.results').html('');
                    if (res.length == 0) return;
                    $('.user-results').show();
                    // Append new results
                    for (i = 0; i < res.length; i++) {
                        var user = res[i];
                        var chip = createChip(user.name, '/User/UserProfile/' + user.id, user.image)
                        var oldHtml = $('.user-results > div.results').html();
                        $('.user-results > div.results').html(oldHtml + chip);
                    }
                }
            })
        } else {
            $('.user-results').hide();
            $('.category-results').hide();
            $('.witness-card-results').hide();
        }

    });

    // Close menu when click outside of it
    $(document).click(function (event) {
        if (!$(event.target).closest('.side-menu-initial').length) {
            if (menuIsOpen) {
                closeMenu();
            }
        }
    })

    $('.menu-btn').click(function () {
        toggleMenu();
    });

    $('.menu-content > li.menu-item').click(function () {
        var href = $($(this).children()[0]).attr("href");
        window.location.href = href;
    })

    if (window.location.search == "?showSuggestion=true") {
        console.log("Showing modal !");
        $('#suggest-modal').modal('open');
    }

    $('#suggest-modal > div.modal-footer > a').click(function () {
        console.log("Closing modal !");
        $('#suggest-modal').modal('close');
    })
});

function goToChipLink(chip) {
    var href = $(chip).find('a').attr('href');
    window.location.pathname = href;  
}

function createChip(name, url, img) {
    if (img != null) return chip = '<div onclick="goToChipLink(this)" class="chip"><a href="' + url + '"><img src="' + img + '" alt="Contact Person">' + name + '</a></div>';
    else return chip = '<div onclick="goToChipLink(this)" class="chip"><a href="' + url + '">' + name + '</a></div>';
}

function toggleMenu() {
    if (menuIsOpen) closeMenu();
    else openMenu();
}

function openMenu() {
    menuIsOpen = true;
    $('.side-menu-initial').animate({
        'width': '250px'
    });
    $('a.brand-logo > img, div.brand-text').animate({
        'left': '-100px'
    });
    $('.menu-content').animate({
        'left': '0px'
    });
}

function closeMenu() {
    menuIsOpen = false;
    $('.side-menu-initial').animate({
        'width': '50px'
    });
    $('a.brand-logo > img, div.brand-text').animate({
        'left': ''
    });
    $('.menu-content').animate({
        'left': '-500px'
    });
}