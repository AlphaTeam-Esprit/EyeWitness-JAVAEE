package tn.esprit.alphateam.notificationEndpoint;

import java.io.Serializable;
import java.util.Date;

import tn.esprit.alphateam.eyewitness.persistence.Notification;

public class NotificationMessage implements Serializable {
	private static final long serialVersionUID = -7930498403828503464L;
	public static final String TYPE_HANDSHAKE = "TYPE_HANDSHAKE";
	public static final String TYPE_MESSAGE = "TYPE_MESSAGE";

	private Integer userId;
	private String type;
	private String message;
	private String imageUrl;
	private Date date;
	private String url;
	private Boolean read;

	public NotificationMessage() {
	}

	public NotificationMessage(Integer userId, String type, String message) {
		super();
		this.userId = userId;
		this.type = type;
		this.message = message;
	}

	public NotificationMessage(Integer userId, String message) {
		super();
		this.userId = userId;
		this.message = message;
		this.type = TYPE_MESSAGE;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isHandshake() {
		return type.equals(TYPE_HANDSHAKE);
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getRead() {
		return read == null ? false : !read ? false : true;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	@Override
	public String toString() {
		return "NotificationMessage [userId=" + userId + ", type=" + type + ", message=" + message + ", imageUrl="
				+ imageUrl + ", date=" + date + ", url=" + url + ", isRead=" + read + "]";
	}
	
}
