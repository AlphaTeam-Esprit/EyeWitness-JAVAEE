package tn.esprit.alphateam.notificationEndpoint;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class NotificationMessageEncoder implements Encoder.Text<NotificationMessage>{

	@Override
	public void init(EndpointConfig config) {
	}

	@Override
	public void destroy() {
	}

	@Override
	public String encode(NotificationMessage message) throws EncodeException {
		 JsonObjectBuilder builder = Json.createObjectBuilder()
							.add("type", message.getType())
							.add("id", message.getUserId())
							.add("message", message.getMessage())
							.add("read", message.getRead());
		 
		 if (message.getDate() != null) builder.add("date", message.getDate().toString());
		 else builder.addNull("date");
		 
		 if (message.getImageUrl() != null) builder.add("imageUrl", message.getImageUrl());
		 else builder.addNull("imageUrl");
		 
		 if (message.getUrl() != null) builder.add("url", message.getUrl());
		 else builder.addNull("url");
		 
		 return builder.build().toString();
	}

}
