package tn.esprit.alphateam.notificationEndpoint;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class NotificationMessageDecoder implements Decoder.Text<NotificationMessage>{

	@Override
	public void init(EndpointConfig config) {
	}

	@Override
	public void destroy() {
	}

	@Override
	public NotificationMessage decode(String message) throws DecodeException {
		NotificationMessage notification = new NotificationMessage();
		JsonObject jsonObject = Json.createReader(new StringReader(message)).readObject();
		notification.setMessage(jsonObject.getString("message"));
		notification.setType(jsonObject.getString("type"));
		notification.setUserId(Integer.valueOf(jsonObject.getString("id")));
		
		if (jsonObject.containsKey("date") && jsonObject.getString("date") != null){
			try {
				notification.setDate(DateFormat.getDateInstance().parse(jsonObject.getString("date")));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if (jsonObject.containsKey("imageUrl") && jsonObject.getString("imageUrl") != null) notification.setImageUrl(jsonObject.getString("imageUrl"));
		if (jsonObject.containsKey("url") && jsonObject.getString("url") != null) notification.setUrl(jsonObject.getString("url"));
		if (jsonObject.containsKey("read") && jsonObject.getString("read") != null) notification.setRead(jsonObject.getBoolean("read"));
		return notification;
	}

	@Override
	public boolean willDecode(String message) {
		boolean willDecode = true;
		
		try {
			
			JsonObject jsonObject = Json.createReader(new StringReader(message)).readObject();
			
			if (!(jsonObject.containsKey("id") && jsonObject.containsKey("type") && jsonObject.containsKey("message"))) {
				willDecode = false;
			}
			
		} catch(Exception e) {
			willDecode = false;
		}
		
		return willDecode;
	}

}
