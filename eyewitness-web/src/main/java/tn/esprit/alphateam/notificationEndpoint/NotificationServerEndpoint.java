package tn.esprit.alphateam.notificationEndpoint;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/notification", encoders = NotificationMessageEncoder.class, decoders = NotificationMessageDecoder.class)
public class NotificationServerEndpoint {

	public static Map<Integer, Session> linkedSessions = Collections.synchronizedMap(new HashMap<Integer, Session>());
	static Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());

	@OnOpen
	public void onOpen(Session session) {
		// tempSessions.add(userSession);
		//System.out.println("OnOpen : " + session.getId());
		sessions.add(session);
	}

	@OnMessage
	public void onMessage(NotificationMessage message, Session session) {
		//System.out.println("OnMessage : " + message);
		
		if (message.isHandshake()) {
			//System.out.println("Message is handshake => linking user id : " + message.getUserId() + " to session : " + session.getId());
			linkedSessions.put(message.getUserId(), session);
			session.getUserProperties().put("userId", message.getUserId());
		} else {
			
		}

	}

	@OnClose
	public void onClose(Session session) {
		//System.out.println("OnClose : " + session.getId());
		linkedSessions.remove(session.getUserProperties().get("userId"));
		sessions.remove(session);
	}
}
