package tn.esprit.alphateam.filters;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.esprit.alphateam.util.JwtServices;

@WebFilter({ "/pages/public/login.jsf", "/pages/public/register.jsf" })
public class GoBackToLoginFilter implements Filter {

	@EJB
	JwtServices jwtServices;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("GoBackToLogin Filter");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		String jwt = jwtServices.getTokenFromRequest(req);
		// User trying to go to login or register page and he is logged in
		if (jwt != null) {
			// Trying to login or register when already logged in
			resp.sendRedirect(req.getContextPath() + "/pages/public/home.jsf");
		} else {
			// Going to login or register not logged in => proceed
			chain.doFilter(request, response);
		}

	}

	@Override
	public void destroy() {

	}

}
