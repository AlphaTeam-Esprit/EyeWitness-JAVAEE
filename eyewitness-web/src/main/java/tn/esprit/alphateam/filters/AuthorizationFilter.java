package tn.esprit.alphateam.filters;

import java.io.IOException;
import java.util.HashMap;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.Claims;
import tn.esprit.alphateam.util.JwtServices;

@WebFilter("/pages/secure/*")
public class AuthorizationFilter implements Filter {

	@EJB
	private JwtServices jwtServices;

	private HashMap<String, String[]> accessPrivileges = new HashMap<>();

	public AuthorizationFilter() {

		accessPrivileges.put("ADMIN", new String[] { "admin", "agent", "user" });
		accessPrivileges.put("AGENT", new String[] { "agent", "user" });
		accessPrivileges.put("USER", new String[] { "user" });
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("Authorization Filter");
		try {

			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;

			String reqURI = req.getRequestURI();

			String jwt = jwtServices.getTokenFromRequest(req);

			// Is JWT present in request
			if (jwt != null) {

				// JWT Found User is logged in!
				System.out.println("JWT FOUND");
				try {

					Claims claims = jwtServices.validateToken(jwt);

					String role = (String) claims.get("role");
					System.out.println("Has user of role : " + role + " access on : " + reqURI);
					// Is user of jwt of authorized role
					if (hasAccess(reqURI, role)) {
						// Authorized access !
						chain.doFilter(request, response);

					} else {
						// Unauthorized access!
						resp.sendRedirect(req.getContextPath() + "/pages/public/error/error.jsf");
					}

				} catch (Exception e) {

					// JWT Has been altered !
					e.printStackTrace();
					resp.sendRedirect(req.getContextPath() + "/pages/public/error/error.jsf");
				}

			} else { // JWT Not present
				// JWT Not found => User not logged in => Redirect to login page
				// !
				System.out.println("JWT Not found");
				resp.sendRedirect(req.getContextPath() + "/pages/public/login.jsf");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean hasAccess(String uri, String role) {

		String[] accessibleFolders = accessPrivileges.get(role);

		for (int i = 0; i < accessibleFolders.length; i++) {
			if (uri.contains(accessibleFolders[i]))
				return true;
		}
		return false;
	}

	@Override
	public void destroy() {

	}
}