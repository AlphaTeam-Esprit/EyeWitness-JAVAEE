package tn.esprit.alphateam.util;

import java.io.Serializable;

public class Breadcrumb implements Serializable {
	private static final long serialVersionUID = -4236784693544514899L;

	private String text;
	private String url;

	public Breadcrumb(String text, String url) {
		super();
		this.text = text;
		this.url = url;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}