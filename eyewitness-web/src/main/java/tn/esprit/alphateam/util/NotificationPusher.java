package tn.esprit.alphateam.util;

import java.io.IOException;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.websocket.EncodeException;
import javax.websocket.Session;

import tn.esprit.alphateam.eyewitness.persistence.Notification;
import tn.esprit.alphateam.eyewitness.persistence.Notification.NotificationType;
import tn.esprit.alphateam.eyewitness.services.interfaces.NotificationServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.notificationEndpoint.NotificationMessage;
import tn.esprit.alphateam.notificationEndpoint.NotificationServerEndpoint;

@Stateless
public class NotificationPusher implements NotificationPusherRemote, NotificationPusherLocal {

	@EJB
	NotificationServicesLocal notificationServicesLocal;
	
	@EJB
	UserServicesLocal userServicesLocal;
	
	public void sendMessageToUser(NotificationMessage message) {

		Session session = NotificationServerEndpoint.linkedSessions.get(message.getUserId());

		if (session != null) {

			try {
				session.getBasicRemote().sendObject(message);
			} catch (IOException | EncodeException e) {
				e.printStackTrace();
			}
		} 
		
		Notification notification = new Notification();
		notification.setDate(new Date());
		notification.setImageUrl(message.getImageUrl());
		notification.setMessage(message.getMessage());
		notification.setRead(false);
		notification.setType(NotificationType.valueOf(message.getType()));
		notification.setUrl(message.getUrl());
		notification.setUser(userServicesLocal.findUserById(message.getUserId()));
		
		notificationServicesLocal.saveNotification(notification);
	}

	@Override
	public void sendMessageToUsers(NotificationMessage message, int... ids) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendMessageToUsersOfRole(NotificationMessage message, String... roles) {
		// TODO Auto-generated method stub

	}

}
