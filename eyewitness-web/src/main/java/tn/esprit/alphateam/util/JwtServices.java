package tn.esprit.alphateam.util;

import java.security.Key;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import tn.esprit.alphateam.eyewitness.persistence.User;

/**
 * Session Bean implementation class JwtServices
 */
@Singleton
@Startup
@LocalBean
public class JwtServices {
	
	private Key key;

	/**
	 * Default constructor.
	 */
	public JwtServices() {
	}

	@PostConstruct
	public void init() {
    	key  = MacProvider.generateKey();
    }

	public String generateToken(User user) {

		String compactJws = Jwts.builder().claim("role", user.getRole())
				.setId(Integer.toString(user.getId()))
				.signWith(SignatureAlgorithm.HS512, key)
				.compact();
		return compactJws;
	}

	// Throws exception when jwt is null
	public Claims validateToken(String jwt) throws IllegalArgumentException{
		Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(jwt);
		return claims.getBody();
	}

	public String getTokenFromRequest(HttpServletRequest request) {
		Cookie jwtCookie = CookieHelper.getCookie("access_token", request);
		return jwtCookie == null ? null : jwtCookie.getValue();
	}
	
	public Cookie getTokenCookieFromRequest(HttpServletRequest request) {
		return CookieHelper.getCookie("access_token", request);
	}
	
	public void setTokenForCurrentUser(String jwt) {
		CookieHelper.setCookie("access_token", jwt, 3600);
	}
	
	public void invalidateToken() {
		CookieHelper.setCookie("access_token", "", 0);
	}
	
}
