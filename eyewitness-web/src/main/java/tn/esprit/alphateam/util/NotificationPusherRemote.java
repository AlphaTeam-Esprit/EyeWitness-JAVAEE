package tn.esprit.alphateam.util;

import javax.ejb.Remote;

import tn.esprit.alphateam.notificationEndpoint.NotificationMessage;

@Remote
public interface NotificationPusherRemote {
	void sendMessageToUser(NotificationMessage message);
	void sendMessageToUsers(NotificationMessage message, int... ids);
	void sendMessageToUsersOfRole(NotificationMessage message, String... roles);
}
