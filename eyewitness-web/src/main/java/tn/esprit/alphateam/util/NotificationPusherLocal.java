package tn.esprit.alphateam.util;

import javax.ejb.Local;

import tn.esprit.alphateam.notificationEndpoint.NotificationMessage;

@Local
public interface NotificationPusherLocal {
	void sendMessageToUser(NotificationMessage message);
	void sendMessageToUsers(NotificationMessage message, int... ids);
	void sendMessageToUsersOfRole(NotificationMessage message, String... roles);
}
