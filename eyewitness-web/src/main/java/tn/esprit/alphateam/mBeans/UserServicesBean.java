package tn.esprit.alphateam.mBeans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureException;
import tn.esprit.alphateam.eyewitness.persistence.AgentRequest;
import tn.esprit.alphateam.eyewitness.persistence.Notification;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.services.interfaces.NotificationServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.util.JwtServices;

@ManagedBean
@ViewScoped
public class UserServicesBean implements Serializable{

	private User user = new User();
	private Part userAvatar;
	private List<Notification> currentUserNotifications;

	private List<User> bannableUsers = new ArrayList<>();
	private List<User> bannedUsers = new ArrayList<>();
	private List<User> agents = new ArrayList<>();
	private int durationDays;
	private AgentRequest agentRequest = new AgentRequest();
	private List<AgentRequest> agentRequests = new ArrayList<>();
	private List<AgentRequest> agentRequestValid= new ArrayList<>();
	
	@EJB
	private UserServicesLocal userServicesLocal;

	@EJB
	private JwtServices jwtServices;
	
	@EJB
	private NotificationServicesLocal notificationServicesLocal;
	
	@PostConstruct
	public void init() {
		User user = getCurrentUser();
		
		if (user != null) {
			currentUserNotifications = notificationServicesLocal.findFiveLastNotificationsByUserId(user.getId());
			doFindBannableUser();
			doFindBannedUser();
			doFindAgentRequest();
			doFindAgentRequestValid();
		}
		
	}
	
	public String doRemoteLogin() {
		String returnUrl = "";
		Client client = ClientBuilder.newClient();

		WebTarget target = client.target("http://localhost:50279/api/UserApi")
				.path("Login")
				.queryParam("email", user.getEmail())
				.queryParam("password", user.getPassword());
		
		Response response = target.request().get();

		User loggedInUser = response.readEntity(User.class);

		if (loggedInUser != null) {

			// Generate JWT
			String jwt = null;
			jwt = jwtServices.generateToken(loggedInUser);

			jwtServices.setTokenForCurrentUser(jwt);

			if (loggedInUser.getRole().equals("ADMIN"))
				returnUrl = "/pages/secure/admin/tab?faces-redirect=true";
			else
				returnUrl = "/pages/public/home?faces-redirect=true";
			
		} else {
			System.out.println("LOGIN ERROR");
		}

		response.close();
		client.close();
		
		return returnUrl;
	}

	public String doLogin() {
		System.out.println("Doing login !");
		String returnUrl = "";

		User loggedInUser = userServicesLocal.login(user.getEmail(), user.getPassword());

		if (loggedInUser != null) {

			// Generate JWT
			String jwt = null;
			jwt = jwtServices.generateToken(loggedInUser);

			jwtServices.setTokenForCurrentUser(jwt);

			if (loggedInUser.getRole().equals("ADMIN"))
				returnUrl = "/pages/secure/admin/admin?faces-redirect=true";
			else
				returnUrl = "/pages/public/home?faces-redirect=true";
			
		} else {
			System.out.println("LOGIN ERROR");
		}

		return returnUrl;
	}

	public String doRegister() {
		
		// Upload images to Content folder configured on wildfly undertow subsystem
		String avatarsPath = System.getProperty("jboss.home.dir") + "\\eyewitness-content\\avatars";
		String fileName = userAvatar.getSubmittedFileName();
		String fileExtension = fileName.substring(fileName.lastIndexOf("."), fileName.length());
		String filePath = user.getEmail() + "_" + user.getId() + fileExtension;
		
		try (InputStream input = userAvatar.getInputStream()) {
	        Files.copy(input, new File(avatarsPath, filePath).toPath());
	    }
	    catch (IOException e) {
	    	e.printStackTrace();
	    }

		user.setAvatar(filePath);
		userServicesLocal.register(user);

		return "/pages/public/login?faces-redirect=true";
	}

	public String logout() {
		String returnUrl = "/pages/public/home?faces-redirect=true";

		// Remove JWT
		jwtServices.invalidateToken();

		return returnUrl;
	}
	
	public void doSendEmailToUser(String message, String subject, String userEmail) {
		userServicesLocal.sendEmailToUser(message, subject, userEmail);
	}
	
	public void doFindBannableUser() {

		bannableUsers = userServicesLocal.findBannableUsers();

	}

	public String doAssignAgent() {

		userServicesLocal.assignAgent(agentRequest);
		return "/pages/secure/admin/tab?faces-redirect=true";

	}

	public String doBanUser() {
       System.out.println(durationDays);
		userServicesLocal.banUser(user.getId(), durationDays);
		userServicesLocal.sendEmailToUser("You have been banned for "+ durationDays + " days", "BAN FROM EYEWITNESS",
				user.getEmail());
		return "/pages/secure/admin/tab?faces-redirect=true";
	}

	public void doFindBannedUser() {

		bannedUsers = userServicesLocal.findBannedUsers();
	}

	public String doUnbanUser() {
		userServicesLocal.unbanUser(user.getId());
		return "/pages/secure/admin/tab?faces-redirect=true";

	}
	

	public void doFindAgents() {

		agents = userServicesLocal.findAgents();

	}

	public String doDemoteAgent() {

		userServicesLocal.demoteAgent(agentRequest);
		return "/pages/secure/admin/tab?faces-redirect=true";

	}

	public void doFindAgentRequestValid() {

		agentRequestValid = userServicesLocal.findAgentRequestActive();

	}

	public void doFindAgentRequest() {

		agentRequests = userServicesLocal.findAgentRequest();

	}

	public User getCurrentUser() {
		User user = null;
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();

			String jwt = jwtServices.getTokenFromRequest(request);

			String id = null;
			if (jwt != null) {
				try {

					Claims claims = jwtServices.validateToken(jwt);

					id = claims.getId();
				} catch (SignatureException e) {

					System.err.println("Can't trust token !");
					e.printStackTrace();
				} catch (Exception e) {
					System.err.println("Token is null => no current User");
					return user;
				}

				user = userServicesLocal.findUserById(Integer.parseInt(id));
			}

		} catch (Exception e) {
			System.err.println("Error while getting user !");
			e.printStackTrace();
		}

		return user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Part getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(Part userAvatar) {
		this.userAvatar = userAvatar;
	}

	public List<Notification> getCurrentUserNotifications() {
		return currentUserNotifications;
	}

	public void setCurrentUserNotifications(List<Notification> currentUserNotifications) {
		this.currentUserNotifications = currentUserNotifications;
	}

	public List<User> getBannableUsers() {
		return bannableUsers;
	}

	public void setBannableUsers(List<User> bannableUsers) {
		this.bannableUsers = bannableUsers;
	}

	public List<User> getBannedUsers() {
		return bannedUsers;
	}

	public void setBannedUsers(List<User> bannedUsers) {
		this.bannedUsers = bannedUsers;
	}

	public List<User> getAgents() {
		return agents;
	}

	public void setAgents(List<User> agents) {
		this.agents = agents;
	}

	public int getDurationDays() {
		return durationDays;
	}

	public void setDurationDays(int durationDays) {
		this.durationDays = durationDays;
	}

	public AgentRequest getAgentRequest() {
		return agentRequest;
	}

	public void setAgentRequest(AgentRequest agentRequest) {
		this.agentRequest = agentRequest;
	}

	public List<AgentRequest> getAgentRequests() {
		return agentRequests;
	}

	public void setAgentRequests(List<AgentRequest> agentRequests) {
		this.agentRequests = agentRequests;
	}

	public List<AgentRequest> getAgentRequestValid() {
		return agentRequestValid;
	}

	public void setAgentRequestValid(List<AgentRequest> agentRequestValid) {
		this.agentRequestValid = agentRequestValid;
	}
	
}
