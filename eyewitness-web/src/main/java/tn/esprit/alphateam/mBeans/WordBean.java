package tn.esprit.alphateam.mBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.persistence.Word;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WordServicesLocal;
import tn.esprit.bank.services.implementation.ClientServices;
import tn.esprit.bank.services.implementation.ClientServicesService;
import tn.esprit.bank.services.implementation.NamingException_Exception;

@ManagedBean
@SessionScoped
public class WordBean {

	private Word word = new Word();
	private Integer idWitnessCard;
	private WitnessCard witnesscard = new WitnessCard();
	private List<Word> AllWords = new ArrayList<Word>();
	private List<Integer> result = new ArrayList<>();
	private Boolean displayWordManage = true;
	private Boolean displayWordCreate = false;
	private Boolean displayWordUpdate = false;
	private Boolean displayPayError = false;
	private String CodeClient;
	private Integer CvvClient;

	@EJB
	WordServicesLocal wordServicesLocal;
	@EJB
	WitnessCardServicesLocal witnessCardServicesLocal;

	public List<Float> doChart() {
		List<Float> floats = wordServicesLocal.analyseWitnessCard(idWitnessCard);
		System.out.println(floats.size());
		return floats;
	}

	public String doCreateOrUpdate() {
		wordServicesLocal.saveAndUpdate(word);
		return cancel();
	}

	public String doDelete() {
		wordServicesLocal.removeWord(word);
		return cancel();
	}

	public String cancel() {
		displayWordUpdate = false;
		displayWordCreate = false;
		displayWordManage = true;
		word = new Word();
		AllWords = wordServicesLocal.getAllWords();
		return "";
	}

	public String doSelectWord() {
		displayWordUpdate = true;
		displayWordManage = false;
		displayWordCreate = false;
		return null;
	}

	public String doNewWord() {
		displayWordCreate = true;
		displayWordUpdate = false;
		displayWordManage = false;
		return null;
	}

	public String doPayForReport() throws NamingException_Exception {
		ClientServicesService clientProxy = new ClientServicesService();
		ClientServices clientServices = clientProxy.getClientServicesPort();
		if (clientServices.payment(CodeClient, CvvClient, witnesscard.getName())) {
			displayPayError = false;
			witnesscard.setIsPayed(true);
			witnessCardServicesLocal.saveOrUpdateWitnessCard(witnesscard);
			return "/pages/secure/agent/manageWitnessCards?faces-redirect=true"; // to report view
		}else{
			displayPayError = true;
			return ""; // error msg check you account
			}

	}

	public String doRedirectToPay() {
		return "/pages/secure/agent/Payment?faces-redirect=true";
	}

	@PostConstruct
	public void init() {
		AllWords = wordServicesLocal.getAllWords();

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		if (req.getQueryString() != null) {
			String idwc = req.getQueryString();
			idWitnessCard = Integer.parseInt(idwc.substring(idwc.indexOf("=") + 1));
			System.out.println("Witness card id is : " + idWitnessCard);
			witnesscard = wordServicesLocal.getWitnessCard(idWitnessCard);
		}
	}

	public Word getWord() {
		return word;
	}

	public void setWord(Word word) {
		this.word = word;
	}

	public List<Integer> getResult() {
		return result;
	}

	public void setResult(List<Integer> result) {
		this.result = result;
	}

	public List<Word> getAllWords() {
		return AllWords;
	}

	public void setAllWords(List<Word> allWords) {
		AllWords = allWords;
	}

	public Boolean getDisplayWordCreate() {
		return displayWordCreate;
	}

	public void setDisplayWordCreate(Boolean displayWordCreate) {
		this.displayWordCreate = displayWordCreate;
	}

	public Boolean getDisplayWordManage() {
		return displayWordManage;
	}

	public void setDisplayWordManage(Boolean displayWordManage) {
		this.displayWordManage = displayWordManage;
	}

	public Boolean getDisplayWordUpdate() {
		return displayWordUpdate;
	}

	public void setDisplayWordUpdate(Boolean displayWordUpdate) {
		this.displayWordUpdate = displayWordUpdate;
	}

	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

	public Integer getIdWitnessCard() {
		return idWitnessCard;
	}

	public void setIdWitnessCard(Integer idWitnessCard) {
		this.idWitnessCard = idWitnessCard;
	}

	public String getCodeClient() {
		return CodeClient;
	}

	public void setCodeClient(String codeClient) {
		CodeClient = codeClient;
	}

	public Integer getCvvClient() {
		return CvvClient;
	}

	public void setCvvClient(Integer cvvClient) {
		CvvClient = cvvClient;
	}

	public Boolean getDisplayPayError() {
		return displayPayError;
	}

	public void setDisplayPayError(Boolean displayPayError) {
		this.displayPayError = displayPayError;
	}
}
