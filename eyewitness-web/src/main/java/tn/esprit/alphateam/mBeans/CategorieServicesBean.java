package tn.esprit.alphateam.mBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.alphateam.eyewitness.persistence.Category;
import tn.esprit.alphateam.eyewitness.services.interfaces.CategorieServicesLocal;

@ManagedBean
@ViewScoped
public class CategorieServicesBean {
	
	private Category categorySelected = new Category();
	
	private List<Category> categories = new ArrayList<>();
	
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	@EJB
	
	CategorieServicesLocal categorieServicesLocal;
	@PostConstruct
	public void init(){
		
		doFindCategoriesByStatus();
		
	}
	
	
	public void doFindCategoriesByStatus(){
		
		
		categories = categorieServicesLocal.findCategoriesByStatus();
	}
	
	public String doValidateCategorie(){
		
		categorieServicesLocal.validateCategorie(categorySelected.getId());
		return "/pages/secure/admin/tab?faces-redirect=true";
		
		
	}
	public String doDeleteCategorie(){
		
		categorieServicesLocal.deleteCategorie(categorySelected);
		return "/pages/secure/admin/tab?faces-redirect=true";
		
	}
	
	

	public Category getCategory() {
		return categorySelected;
	}

	public void setCategory(Category category) {
		this.categorySelected = category;
	}
	

}
