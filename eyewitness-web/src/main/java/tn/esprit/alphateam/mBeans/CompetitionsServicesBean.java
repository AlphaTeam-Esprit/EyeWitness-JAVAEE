package tn.esprit.alphateam.mBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import tn.esprit.alphateam.eyewitness.persistence.Competition;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.CompetitionsServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;
import tn.esprit.alphateam.util.MessageUtil;

@ManagedBean
@ViewScoped
public class CompetitionsServicesBean {
	@EJB
	CompetitionsServicesLocal competitionsServicesLocal;

	@EJB
	WitnessCardServicesLocal witnesscardServicesLocal;

	private List<Competition> competitionList = new ArrayList<Competition>();
	private List<WitnessCard> witnesscardList = new ArrayList<>();
	private Competition competition = new Competition();
	private List<SelectItem> selectItmeOneWitnessCard;
	private WitnessCard witnesscard = new WitnessCard();
	private List<Competition> filteredCompetitions;

	public List<Competition> getFilteredCompetitions() {
		return filteredCompetitions;
	}

	public void setFilteredCompetitions(List<Competition> filteredCompetitions) {
		this.filteredCompetitions = filteredCompetitions;
	}

	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

	public List<SelectItem> getSelectItmeOneWitnessCard() {
		return selectItmeOneWitnessCard;
	}

	public void setSelectItmeOneWitnessCard(List<SelectItem> selectItmeOneWitnessCard) {
		this.selectItmeOneWitnessCard = selectItmeOneWitnessCard;
	}

	public List<Competition> getCompetitionList() {
		return competitionList;
	}

	public void setCompetitionList(List<Competition> competitionList) {
		this.competitionList = competitionList;
	}

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public void doFindAllCompetitions() {

		this.competitionList = competitionsServicesLocal.findAllCompetitions();
	}
	@PostConstruct
	public void init() {
		
		competitionList = competitionsServicesLocal.findAllCompetitions();
		// System.out.print(witnesscards.size());
	}
	public void doFindCompetitions() {

		this.witnesscardList = competitionsServicesLocal.findWitnessCards();
	}

	public void doFindCompetitionById() {
		this.competition = competitionsServicesLocal.findCompetitionById(this.competition.getId());
	}

	public void clearCompetition() {
		this.competition = new Competition();
	}

	public List<WitnessCard> getWitnesscardList() {
		return witnesscardList;
	}

	public void setWitnesscardList(List<WitnessCard> witnesscardList) {
		this.witnesscardList = witnesscardList;
	}

	public String doDeleteCompetition(Competition compe) {

		try {
			competitionsServicesLocal.deleteCompetition(compe);
			MessageUtil.addSuccessMessage("Post was successfully deleted.");
		} catch (Exception e) {
			MessageUtil.addErrorMessage("Could not delete competition.");
		}

		return "index";
	}

	public String doCreateCompetition(WitnessCard wc) {
		try {
			//User u = (User) witnesscard.getCreator();
			//witnesscard=witnesscardServicesLocal.findWitnessById(3);
			//witnesscardServicesLocal.add(witnesscard);
			competition.setWitnesscard(wc);
			competitionsServicesLocal.createCompetition(this.competition);
			MessageUtil.addSuccessMessage("Post was successfully created.");
		} catch (Exception e) {
			MessageUtil.addErrorMessage("Post could not be saved. A Persisting error occured.");
		}

		return "index";
	}

	public String doUpdateCompetition() {
		try {
			competitionsServicesLocal.updateCompetition(this.competition);
			MessageUtil.addSuccessMessage(
					"Competition with id " + this.competition.getId() + " was successfully updated.");

		} catch (Exception e) {
			MessageUtil.addErrorMessage("Competition with id " + this.competition.getId()
					+ " could not be saved. An update error occured.");
		}

		return "index";
	}
	 public void save() {
	        FacesContext.getCurrentInstance().addMessage(null,
	                new FacesMessage("Welcome " + competition.getTheme()));
	    }

}
