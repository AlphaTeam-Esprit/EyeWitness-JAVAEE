package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.alphateam.eyewitness.persistence.ForumSection;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesLocal;
import tn.esprit.alphateam.util.Breadcrumb;

@ManagedBean
@ViewScoped
public class ForumServicesBean implements Serializable {
	private static final long serialVersionUID = 3431478683801122740L;

	// Services
	@EJB
	ForumServicesLocal forumServicesLocal;
	
	// Models
	private List<ForumTopic> hotTopics = new ArrayList<>();
	private List<ForumSection> sections = new ArrayList<>();
	private List<Breadcrumb> breadcrumbs = new ArrayList<>();
	
	public ForumServicesBean() {}
	
	@PostConstruct
	public void init() {
		hotTopics = forumServicesLocal.findHotTopics();
		sections = forumServicesLocal.findAllSections();
		breadcrumbs.add(new Breadcrumb("Home", "/eyewitness-web/pages/public/forum/home.jsf"));
	}
	
	// Recalls
	public String showSubject(ForumSubject subject) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("subject", subject);
		return "/pages/public/forum/subject.jsf?faces-redirect=true";
	}
	
	public int doFindNumberOfMessagesBySubject(ForumSubject subject) {
		return forumServicesLocal.findNumberOfMessagesBySubjectId(subject.getId());
	}
	
	public ForumTopic doFindLatestTopicBySubject(ForumSubject subject){
		return forumServicesLocal.findLatestTopicBySubjectId(subject.getId());
	}

	// Getters and setters
	public List<ForumTopic> getHotTopics() {
		return hotTopics;
	}

	public void setHotTopics(List<ForumTopic> hotTopics) {
		this.hotTopics = hotTopics;
	}

	public List<ForumSection> getSections() {
		return sections;
	}

	public void setSections(List<ForumSection> sections) {
		this.sections = sections;
	}

	public List<Breadcrumb> getBreadcrumbs() {
		return breadcrumbs;
	}

	public void setBreadcrumbs(List<Breadcrumb> breadcrumbs) {
		this.breadcrumbs = breadcrumbs;
	}
	
}
