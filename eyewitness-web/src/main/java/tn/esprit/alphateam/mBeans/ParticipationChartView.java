package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.EventServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;

@ManagedBean
public class ParticipationChartView implements Serializable {
	@EJB
	private EventServicesLocal eventcardServicesLocal;
	@EJB
	private UserServicesLocal userServicesLocal;
	private BarChartModel barModel;
	private HorizontalBarChartModel horizontalBarModel;
	private HashMap<Integer, WitnessCard> hashmap = new HashMap<>();
	private int numberOfUsers;

	@PostConstruct
	public void init() {
		hashmap = eventcardServicesLocal.numberOfParticipatedUserToEventWitnesscard();
		numberOfUsers = userServicesLocal.findAllUsers().size();
		createBarModels();
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public HorizontalBarChartModel getHorizontalBarModel() {
		return horizontalBarModel;
	}

	private BarChartModel initBarModel() {

		BarChartModel model = new BarChartModel();
		ChartSeries participated = new ChartSeries();
		participated.setLabel("Participated");
		ChartSeries unparticipated = new ChartSeries();
		unparticipated.setLabel("UnParticipated");

		for (Iterator ii = hashmap.keySet().iterator(); ii.hasNext();)

		{
			int key = (int) ii.next();
			WitnessCard value = hashmap.get(key);

			participated.set(value.getName(), key);
			unparticipated.set(value.getName(), numberOfUsers - key);

		}

		model.addSeries(participated);
		model.addSeries(unparticipated);

		return model;
	}

	private void createBarModels() {
		createBarModel();
		createHorizontalBarModel();
	}

	private void createBarModel() {
		barModel = initBarModel();

		barModel.setTitle("Bar Chart Number Of Persons Participated/UnParticipated Event relating to witnesscard ");
	
		Axis xAxis = barModel.getAxis(AxisType.X);
		xAxis.setLabel("Witnesscards");

		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setLabel("Number of Persons");
		yAxis.setMin(0);
		yAxis.setMax(8);

	}

	private void createHorizontalBarModel() {
		horizontalBarModel = new HorizontalBarChartModel();

		ChartSeries boys = new ChartSeries();
		boys.setLabel("Boys");
		boys.set("2004", 50);
		boys.set("2005", 96);
		boys.set("2006", 44);
		boys.set("2007", 55);
		boys.set("2008", 25);

		ChartSeries girls = new ChartSeries();
		girls.setLabel("Girls");
		girls.set("2004", 52);
		girls.set("2005", 60);
		girls.set("2006", 82);
		girls.set("2007", 35);
		girls.set("2008", 120);

		horizontalBarModel.addSeries(boys);
		horizontalBarModel.addSeries(girls);

		horizontalBarModel.setTitle("Horizontal and Stacked");
		horizontalBarModel.setLegendPosition("ne");
		horizontalBarModel.setStacked(true);

		Axis xAxis = horizontalBarModel.getAxis(AxisType.X);
		xAxis.setLabel("Births");
		xAxis.setMin(0);
		xAxis.setMax(200);

		Axis yAxis = horizontalBarModel.getAxis(AxisType.Y);
		yAxis.setLabel("Gender");
	}

	public HashMap<Integer, WitnessCard> getHashmap() {
		return hashmap;
	}

	public void setHashmap(HashMap<Integer, WitnessCard> hashmap) {
		this.hashmap = hashmap;
	}

	public int getNumberOfUsers() {
		return numberOfUsers;
	}

	public void setNumberOfUsers(int numberOfUsers) {
		this.numberOfUsers = numberOfUsers;
	}

}
