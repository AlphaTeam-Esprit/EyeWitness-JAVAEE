package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.esprit.alphateam.eyewitness.persistence.ForumMessage;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic.ForumTopicState;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;
import tn.esprit.alphateam.util.Breadcrumb;

@ManagedBean
@ViewScoped
public class ForumTopicCreationBean implements Serializable {
	private static final long serialVersionUID = -4864216401020056996L;

	private Integer subjectId;
	private ForumSubject currentSubject = new ForumSubject();
	private ForumTopic currentTopic = new ForumTopic();
	private ForumMessage currentMessage = new ForumMessage();
	private WitnessCard choosenCard = new WitnessCard();
	private List<WitnessCard> witnesscards = new ArrayList<>();
	
	@EJB
	private ForumServicesLocal forumServicesLocal;
	@ManagedProperty("#{forumServicesBean}")
	private ForumServicesBean forumServicesBean;
	@ManagedProperty("#{userServicesBean}")
	private UserServicesBean userServicesBean;
	
	@EJB
	WitnessCardServicesLocal witnessCardServicesLocal;
	
	@PostConstruct
	public void init() {
		
		if (subjectId != null) {
			
			System.out.println("Current subject id is " + subjectId);
			currentSubject = forumServicesLocal.findSubjectById(subjectId);
			witnesscards = witnessCardServicesLocal.listWitnessCards();
			forumServicesBean.getBreadcrumbs().add(new Breadcrumb(currentSubject.getTitle(), ""));
			forumServicesBean.getBreadcrumbs().add(new Breadcrumb("Create a topic", ""));
		} else {
			// Error
			System.err.println("Subject id is null !");
		}
		
	}
	
	public String createTopic() {
		User loggedInUser = userServicesBean.getCurrentUser();
		currentTopic.setCreator(loggedInUser);
		WitnessCard card = witnessCardServicesLocal.findWitnessCardById(choosenCard.getId());
		currentTopic.setAboutWitnessCard(card);
		currentTopic.setCreationDate(new Date());
		currentTopic.setForumSubject(currentSubject);
		currentTopic.setIsPinned(false);
		currentTopic.setState(ForumTopicState.ONGOING);
		
		List<ForumMessage> messages = new ArrayList<>();
		currentMessage.setAuthor(loggedInUser);
		currentMessage.setForumTopic(currentTopic);
		currentMessage.setPostDate(new Date());
		messages.add(currentMessage);
		
		currentTopic.setForumMessages(messages);
		
		forumServicesLocal.saveOrUpdateTopic(currentTopic);
		
		
		return "";
	}
	
	public UserServicesBean getUserServicesBean() {
		return userServicesBean;
	}

	public void setUserServicesBean(UserServicesBean userServicesBean) {
		this.userServicesBean = userServicesBean;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public ForumSubject getCurrentSubject() {
		return currentSubject;
	}

	public void setCurrentSubject(ForumSubject currentSubject) {
		this.currentSubject = currentSubject;
	}

	public ForumTopic getCurrentTopic() {
		return currentTopic;
	}

	public void setCurrentTopic(ForumTopic currentTopic) {
		this.currentTopic = currentTopic;
	}

	public ForumServicesBean getForumServicesBean() {
		return forumServicesBean;
	}

	public void setForumServicesBean(ForumServicesBean forumServicesBean) {
		this.forumServicesBean = forumServicesBean;
	}

	public ForumMessage getCurrentMessage() {
		return currentMessage;
	}

	public void setCurrentMessage(ForumMessage currentMessage) {
		this.currentMessage = currentMessage;
	}

	public WitnessCard getChoosenCard() {
		return choosenCard;
	}

	public void setChoosenCard(WitnessCard choosenCard) {
		this.choosenCard = choosenCard;
	}

	public List<WitnessCard> getWitnesscards() {
		return witnesscards;
	}

	public void setWitnesscards(List<WitnessCard> witnesscards) {
		this.witnesscards = witnesscards;
	}
	
}
