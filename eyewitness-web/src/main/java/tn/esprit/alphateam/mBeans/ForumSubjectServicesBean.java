package tn.esprit.alphateam.mBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesLocal;
import tn.esprit.alphateam.util.Breadcrumb;

@ManagedBean
@ViewScoped
public class ForumSubjectServicesBean implements Serializable {
	private static final long serialVersionUID = 6897192884856449922L;

	// Services
	@EJB
	private ForumServicesLocal forumServicesLocal;
	@ManagedProperty("#{forumServicesBean}")
	private ForumServicesBean forumServicesBean;

	// Models
	private Integer subjectId;
	private ForumSubject currentSubject = new ForumSubject();

	public ForumSubjectServicesBean() {
	}

	@PostConstruct
	public void init() {

		if (subjectId != null) {
			currentSubject = forumServicesLocal.findSubjectById(subjectId);
			forumServicesBean.getBreadcrumbs().add(new Breadcrumb(currentSubject.getTitle(),
					"/eyewitness-web/pages/public/forum/subject.jsf?id=" + subjectId.toString()));
		} else {
			// Error message !
		}

	}

	// Recalls
	public String followCurrentSubject() {
		return "";
	}

	// Getters and setters
	public ForumSubject getCurrentSubject() {
		return currentSubject;
	}

	public void setCurrentSubject(ForumSubject currentSubject) {
		this.currentSubject = currentSubject;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public ForumServicesBean getForumServicesBean() {
		return forumServicesBean;
	}

	public void setForumServicesBean(ForumServicesBean forumServicesBean) {
		this.forumServicesBean = forumServicesBean;
	}

}
