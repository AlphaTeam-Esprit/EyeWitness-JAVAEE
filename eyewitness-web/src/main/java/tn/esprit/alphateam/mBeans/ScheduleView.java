package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import tn.esprit.alphateam.eyewitness.persistence.Event;
import tn.esprit.alphateam.eyewitness.services.interfaces.EventServicesLocal;

@ManagedBean
@ViewScoped
public class ScheduleView implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	EventServicesLocal eventServicesLocal;
	
	@ManagedProperty("#{userServicesBean}")
	UserServicesBean userServicesBean;
	
	private ScheduleModel eventModel;

	private ScheduleEvent event = new DefaultScheduleEvent();

	@PostConstruct
	public void init() {
		eventModel = new DefaultScheduleModel();
		List<Event> events = eventServicesLocal.findEventsByAgentId(userServicesBean.getCurrentUser().getId());
		for (Event e : events) {
			DefaultScheduleEvent ev = new DefaultScheduleEvent(e.getName(), e.getDateFrom(), e.getDateTo());
			ev.setData(e);
			eventModel.addEvent(ev);
		}
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public void addEvent(ActionEvent actionEvent) {

		DefaultScheduleEvent ev = (DefaultScheduleEvent) eventModel.getEvent(event.getId());
		Event e = eventServicesLocal.findEventById(((Event)ev.getData()).getId());
		
		e.setDateFrom(ev.getStartDate());
		e.setDateTo(ev.getEndDate());
		e.setName(ev.getTitle());
		
		eventServicesLocal.saveOrUpdateEvent(e);
		
		eventModel.updateEvent(event);

		event = new DefaultScheduleEvent();
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
	}

	public void onDateSelect(SelectEvent selectEvent) {
		event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved",
				"Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

		DefaultScheduleEvent scheduleEvent = (DefaultScheduleEvent) event.getScheduleEvent();
		
		Event ev = (Event) scheduleEvent.getData();
		ev = eventServicesLocal.findEventById(ev.getId());

		ev.setDateFrom(scheduleEvent.getStartDate());
		ev.setDateTo(scheduleEvent.getEndDate());
		
		eventServicesLocal.saveOrUpdateEvent(ev);
		
		addMessage(message);
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized",
				"Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
		
		DefaultScheduleEvent ev = (DefaultScheduleEvent) event.getScheduleEvent();
		
		Event eventEntity = (Event) ev.getData();
		eventEntity = eventServicesLocal.findEventById(eventEntity.getId());
		
		eventEntity.setDateFrom(ev.getStartDate());
		eventEntity.setDateTo(ev.getEndDate());
		
		eventServicesLocal.saveOrUpdateEvent(eventEntity);
		
		addMessage(message);
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public UserServicesBean getUserServicesBean() {
		return userServicesBean;
	}

	public void setUserServicesBean(UserServicesBean userServicesBean) {
		this.userServicesBean = userServicesBean;
	}

}