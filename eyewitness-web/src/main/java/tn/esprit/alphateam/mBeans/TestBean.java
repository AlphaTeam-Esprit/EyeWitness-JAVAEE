package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;

@ManagedBean
@ViewScoped
public class TestBean implements Serializable {

	List<WitnessCard> witnesscards = new ArrayList<>();
	WitnessCard witnesscard = new WitnessCard();
	
	@EJB
	WitnessCardServicesLocal witnessCardServicesLocal;
	
	@PostConstruct
	public void init() {
		witnesscards = witnessCardServicesLocal.listWitnessCards();
	}

	public void show() {
		System.out.println("SALEM !");
		System.out.println(witnesscard.getId());
	}
	
	public List<WitnessCard> getWitnesscards() {
		return witnesscards;
	}

	public void setWitnesscards(List<WitnessCard> witnesscards) {
		this.witnesscards = witnesscards;
	}

	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}
	
	
	
}
