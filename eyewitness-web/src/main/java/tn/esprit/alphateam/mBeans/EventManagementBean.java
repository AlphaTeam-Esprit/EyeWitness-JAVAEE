package tn.esprit.alphateam.mBeans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureException;
import tn.esprit.alphateam.eyewitness.persistence.Event;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.EventServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;
import tn.esprit.alphateam.util.JwtServices;

@ManagedBean
@SessionScoped
public class EventManagementBean {
	@EJB
	private EventServicesLocal eventcardServicesLocal;

	@EJB
	private WitnessCardServicesLocal witnesscardServicesLocal;
	private Part eventPicture;
	private WitnessCard witnesscard = new WitnessCard();
	private List<WitnessCard> witnesscards = new ArrayList<>();
	private Event event = new Event();
	private String messageErreur = "";
	private List<Event> events = new ArrayList<>();

	private List<Event> eventsunparticipated = new ArrayList<>();

	@EJB
	private UserServicesLocal userServicesLocal;

	@EJB
	private JwtServices jwtServices;

	@PostConstruct
	public void init() {

		User currentUser = getCurrentUser();
		witnesscards = witnesscardServicesLocal.listWitnessCardsByCurrentAgent(currentUser.getId());
		events = eventcardServicesLocal.listEventsByCurrentAgent(currentUser.getId());
		eventsunparticipated = eventcardServicesLocal.listUnparticipatedEventByUserId(currentUser.getId());

	}

	public String doParticipate(int idUser, Event event) {
		eventcardServicesLocal.participate(idUser, event);
		return "";
	}

	public boolean doVerifyDateParticipation(Event event, int idUser) {
		boolean b = eventcardServicesLocal.verifyDateParticipation(event, idUser);
		return b;
	}

	public List<User> dolistFollowedUsersByEvent(int idEvent, int idUser) {
		return eventcardServicesLocal.listFollowedUsersByEvent(idEvent, idUser);
	}

	public String doAddEvent() {
		// Upload images to Content folder configured on wildfly undertow
		// subsystem
		String avatarsPath = System.getProperty("jboss.home.dir") + "\\eyewitness-content\\";
		String fileName = eventPicture.getSubmittedFileName();
		String fileExtension = fileName.substring(fileName.lastIndexOf("."), fileName.length());

		String filePath = event.getName() + fileExtension;
		try (InputStream input = eventPicture.getInputStream()) {
			Files.copy(input, new File(avatarsPath, filePath).toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (event.getDateFrom().before(event.getDateTo())) {
			event.setPicture(filePath);
			eventcardServicesLocal.addEvent(witnesscard, event);

			return "/pages/secure/agent/manageWitnessCards?faces-redirect=true";
		}

		else {

			return "";
		}
	}

	public String doUpdateEvent() {
		if (event.getDateFrom().before(event.getDateTo())) {
			eventcardServicesLocal.saveOrUpdateEvent(event);
			return "/pages/secure/agent/listEvents?faces-redirect=true";
		} else {

			return "";
		}
	}

	public String doDeleteEvent() {
		if (event.getDateFrom().before(event.getDateTo())) {
			eventcardServicesLocal.deleteEvent(event);
			return "/pages/secure/agent/listEvents?faces-redirect=true";
		} else
			return "";

	}

	public String doUpdateWitnesscard() {
		witnesscardServicesLocal.saveOrUpdateWitnessCard(witnesscard);
		return "/pages/secure/agent/manageWitnessCards?faces-redirect=true";
	}

	public String goToAddEvent(WitnessCard wc) {
		String returnUrl = "";
		witnesscard = wc;
		event = new Event();
		returnUrl = "/pages/secure/agent/createEvent?faces-redirect=true";

		return returnUrl;
	}

	public String goToUpdateEvent(Event ev) {
		String returnUrl = "";
		event = ev;
		returnUrl = "/pages/secure/agent/updateEvent?faces-redirect=true";

		return returnUrl;
	}

	public String goToUpdateWitnesscard(WitnessCard wc) {
		String returnUrl = "";
		witnesscard = wc;
		returnUrl = "/pages/secure/agent/editWitnesscard?faces-redirect=true";

		return returnUrl;
	}

	public String goToDeleteEvent(Event ev) {
		String returnUrl = "";
		event = ev;
		returnUrl = "/pages/secure/agent/deleteEvent?faces-redirect=true";

		return returnUrl;
	}

	public String erreurMessage() {
		if (event.getDateFrom().before(event.getDateTo()))
			return "ok";
		else
			return "";

	}

	public List<Event> getEventsunparticipated() {
		return eventsunparticipated;
	}

	public void setEventsunparticipated(List<Event> eventsunparticipated) {
		this.eventsunparticipated = eventsunparticipated;
	}

	public Event getEvent() {

		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

	public List<WitnessCard> getWitnesscards() {
		return witnesscards;
	}

	public void setWitnesscards(List<WitnessCard> witnesscards) {
		this.witnesscards = witnesscards;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public String getMessageErreur() {
		return messageErreur;
	}

	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}

	public Part getEventPicture() {
		return eventPicture;
	}

	public void setEventPicture(Part eventPicture) {
		this.eventPicture = eventPicture;
	}

	public User getCurrentUser() {
		User user = null;
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();

			String jwt = jwtServices.getTokenFromRequest(request);

			String id = null;
			if (jwt != null) {
				try {

					Claims claims = jwtServices.validateToken(jwt);

					id = claims.getId();
				} catch (SignatureException e) {

					System.err.println("Can't trust token !");
					e.printStackTrace();
				} catch (Exception e) {
					System.err.println("Token is null => no current User");
					return user;
				}

				user = userServicesLocal.findUserById(Integer.parseInt(id));
			}

		} catch (Exception e) {
			System.err.println("Error while getting user !");
			e.printStackTrace();
		}

		return user;
	}

}
