package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.alphateam.eyewitness.persistence.Comments;
import tn.esprit.alphateam.eyewitness.persistence.Competition;
import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.BadWordsServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.CompetitionsServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.EventServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;

@ManagedBean
@ViewScoped
public class WitnessCardsServicesBean implements Serializable {

	private WitnessCard witnesscard = new WitnessCard();
	private List<WitnessCard> witnessCardsSTB = new ArrayList<>();

	private List<WitnessCard> witnessCardsByAgent = new ArrayList<>();
	private Integer selectedUserId;
	private Competition competition = new Competition();
	private List<WitnessCard> witnesscards = new ArrayList<>();
	private List<Competition> competitions = new ArrayList<>();

	String SearchWCString = "";
	boolean displayFormWc = false;
	boolean displayComment = false;
	boolean displayWCDetails = false;
	boolean displaysearch = true;
	List<Comments> commentsOfWitnessCard;
	String commentText;

	@EJB
	WitnessCardServicesLocal witnessCardServicesLocal;
	@EJB
	CompetitionsServicesLocal competitionsServicesLocal;
	@EJB
	private UserServicesLocal userServicesLocal;
	@EJB
	private EventServicesLocal eventServicesLocal;
	@EJB
	BadWordsServicesLocal badWordServicesLocal;
	@ManagedProperty(value = "#{userServicesBean}")
	private UserServicesBean userServicesBean;

	@PostConstruct
	public void init() {
		doFindWitnessCardNonValid();
		competition = (Competition) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("comp");
		witnesscard = (WitnessCard) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("wc");

		witnesscards = witnessCardServicesLocal.listWitnessCards();
		competitions = competitionsServicesLocal.findAllCompetitions();
	}

	public String doparticipateCompetition(Competition competition) {
		String returnUrl = "";
		User user = userServicesBean.getCurrentUser();
		userServicesLocal.participateCompetition(user, competition);

		return returnUrl;
	}

	public String goCreateCompetition() {

		System.out.println(witnesscard);
		System.out.println(competition);

		competitionsServicesLocal.createCompetition(competition);

		return "index";
	}

	public String doCreateCom(WitnessCard wc) {
		String returnUrl = "";

		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("wc", wc);
		returnUrl = "/pages/secure/admin/create?faces-redirect=true";

		return returnUrl;
	}

	public void doFindWitnessCardNonValid() {

		witnessCardsSTB = witnessCardServicesLocal.findNoValidWitnessCard();

	}

	public String doValidateWitnessCard() {
		witnessCardServicesLocal.validateWitnessCard(witnesscard.getId());
		return "/pages/secure/admin/tab?faces-redirect=true";

	}

	public String doDeleteWitnessCard() {

		witnessCardServicesLocal.DeleteWitnessCard(witnesscard);
		return "/pages/secure/admin/tab?faces-redirect=true";
	}

	public UserServicesBean getUserServicesBean() {
		return userServicesBean;
	}

	public void setUserServicesBean(UserServicesBean userServicesBean) {
		this.userServicesBean = userServicesBean;
	}

	public Integer getSelectedUserId() {
		return selectedUserId;
	}

	public void setSelectedUserId(Integer selectedUserId) {
		this.selectedUserId = selectedUserId;
	}

	public void doFindWitnessCardByAgent() {

		witnessCardsByAgent = witnessCardServicesLocal.findWitnessCardsByAgent(selectedUserId);
	}

	public String doSearchWCbyName() {
		witnesscards = witnessCardServicesLocal.getAllWitnessCardsByname(SearchWCString);
		displayFormWc = true;
		return "";
	}

	public String doCheckWitnessCard(WitnessCard w) {

		displayFormWc = false;
		displaysearch = false;
		witnesscard = w;
		commentsOfWitnessCard = witnessCardServicesLocal.getAllCommentsByWitnessCardId(w.getId());
		displayComment = true;

		displayWCDetails = true;

		return "";
	}

	public String doCommenterwintnesscard() {
		int idUser = userServicesBean.getCurrentUser().getId();
		String UserName = userServicesBean.getCurrentUser().getFirstName()
				+ userServicesBean.getCurrentUser().getLastName();
		int idWc = witnesscard.getId();

		Comments comment = new Comments();
		comment.setComment(commentText);
		comment.setIdUser(idUser);
		comment.setIdWitnessCard(idWc);
		comment.setUserName(UserName);

		badWordServicesLocal.CommentAwitnessCard(comment);
		commentsOfWitnessCard = witnessCardServicesLocal.getAllCommentsByWitnessCardId(idWc);
		commentText = "";

		return "";
	}

	public WitnessCard getWitnesscard() {
		return witnesscard;
	}

	public void setWitnesscard(WitnessCard witnesscard) {
		this.witnesscard = witnesscard;
	}

	public List<WitnessCard> getWitnessCardsSTB() {
		return witnessCardsSTB;
	}

	public void setWitnessCardsSTB(List<WitnessCard> witnessCardsSTB) {
		this.witnessCardsSTB = witnessCardsSTB;
	}

	public List<WitnessCard> getWitnessCardsByAgent() {
		return witnessCardsByAgent;
	}

	public void setWitnessCardsByAgent(List<WitnessCard> witnessCardsByAgent) {
		this.witnessCardsByAgent = witnessCardsByAgent;
	}

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public List<WitnessCard> getWitnesscards() {
		return witnesscards;
	}

	public void setWitnesscards(List<WitnessCard> witnesscards) {
		this.witnesscards = witnesscards;
	}

	public List<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}

	public List<WitnessCard> getWitnessCards() {
		return witnesscards;
	}

	public void setWitnessCards(List<WitnessCard> witnessCards) {
		this.witnesscards = witnessCards;
	}

	public String getSearchWCString() {
		return SearchWCString;
	}

	public void setSearchWCString(String searchWCString) {
		SearchWCString = searchWCString;
	}

	public boolean isDisplayFormWc() {
		return displayFormWc;
	}

	public void setDisplayFormWc(boolean displayFormWc) {
		this.displayFormWc = displayFormWc;
	}

	public boolean isDisplayComment() {
		return displayComment;
	}

	public void setDisplayComment(boolean displayComment) {
		this.displayComment = displayComment;
	}

	public boolean isDisplayWCDetails() {
		return displayWCDetails;
	}

	public void setDisplayWCDetails(boolean displayWCDetails) {
		this.displayWCDetails = displayWCDetails;
	}

	public boolean isDisplaysearch() {
		return displaysearch;
	}

	public void setDisplaysearch(boolean displaysearch) {
		this.displaysearch = displaysearch;
	}

	public List<Comments> getCommentsOfWitnessCard() {
		return commentsOfWitnessCard;
	}

	public void setCommentsOfWitnessCard(List<Comments> commentsOfWitnessCard) {
		this.commentsOfWitnessCard = commentsOfWitnessCard;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

}
