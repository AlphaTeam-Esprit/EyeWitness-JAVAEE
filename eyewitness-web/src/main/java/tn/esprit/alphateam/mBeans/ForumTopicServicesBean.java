package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.alphateam.eyewitness.persistence.ForumMessage;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.persistence.ForumTopic;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesLocal;
import tn.esprit.alphateam.util.Breadcrumb;

@ManagedBean
@ViewScoped
public class ForumTopicServicesBean implements Serializable {
	private static final long serialVersionUID = 19052796140718150L;

	// Services
	@EJB
	private ForumServicesLocal forumServicesLocal;
	@ManagedProperty("#{forumServicesBean}")
	private ForumServicesBean forumServicesBean;
	
	// Models
	private Integer topicId;
	private ForumTopic currentTopic = new ForumTopic();
	private List<ForumMessage> currentTopicMessages = new ArrayList<ForumMessage>();
	
	public ForumTopicServicesBean() {}
	
	@PostConstruct
	public void init() {
		
		if (topicId != null) {
			currentTopic = forumServicesLocal.findTopicById(topicId);
			currentTopicMessages = forumServicesLocal.findMessagesByTopicIdOrderByDate(topicId);
			forumServicesBean.getBreadcrumbs().add(new Breadcrumb(currentTopic.getTitle(),
					"/eyewitness-web/pages/public/forum/subject.jsf?id=" + currentTopic.getForumSubject().getId()));
			forumServicesBean.getBreadcrumbs().add(new Breadcrumb(currentTopic.getForumSubject().getTitle(),
					"/eyewitness-web/pages/public/forum/topic.jsf?id=" + topicId));
		} else {
			// Error Message
		}
		
	}

	//Recalls
	public void followCurrentTopic() {
		
	}
	
	public void replyCurrentTopic() {
		
	}
	
	// Getters and setters
	public ForumServicesBean getForumServicesBean() {
		return forumServicesBean;
	}

	public void setForumServicesBean(ForumServicesBean forumServicesBean) {
		this.forumServicesBean = forumServicesBean;
	}

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(Integer topicId) {
		this.topicId = topicId;
	}

	public ForumTopic getCurrentTopic() {
		return currentTopic;
	}

	public void setCurrentTopic(ForumTopic currentTopic) {
		this.currentTopic = currentTopic;
	}

	public List<ForumMessage> getCurrentTopicMessages() {
		return currentTopicMessages;
	}

	public void setCurrentTopicMessages(List<ForumMessage> currentTopicMessages) {
		this.currentTopicMessages = currentTopicMessages;
	}


	
}
