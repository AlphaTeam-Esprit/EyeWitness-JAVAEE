package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.alphateam.eyewitness.persistence.ForumSection;
import tn.esprit.alphateam.eyewitness.persistence.ForumSubject;
import tn.esprit.alphateam.eyewitness.services.interfaces.ForumServicesLocal;

@ManagedBean
@ViewScoped
public class ForumManagementBean implements Serializable {

	// Services
	@EJB
	ForumServicesLocal forumServicesLocal;
	
	// Models
	List<ForumSection> forumSections = new ArrayList<>();
	ForumSection currentSection = new ForumSection();
	ForumSection addingSection = new ForumSection();
	ForumSubject addingSubject = new ForumSubject();
	private boolean addSectionShown = true;

	@PostConstruct
	public void init() {
		forumSections = forumServicesLocal.findAllSections();
		try {
			currentSection = forumSections.get(0);
		} catch (Exception e) {
			System.err.println("Section");
		}
	}
	
	public void addSection() {
		if (addSectionShown) {
			forumServicesLocal.saveOrUpdateSection(addingSection);
			addingSection = new ForumSection();
		}
		addSectionShown = true;
	}
	
	public void addSubject() {
		if (!addSectionShown) {
			addingSubject.setForumSection(currentSection);
			forumServicesLocal.saveOrUpdateSubject(addingSubject);
			
			currentSection = forumServicesLocal.findSectionById(currentSection.getId());
			
			addingSubject = new ForumSubject();
		}
		addSectionShown = false;
	}

	public List<ForumSection> getForumSections() {
		return forumSections;
	}

	public void setForumSections(List<ForumSection> forumSections) {
		this.forumSections = forumSections;
	}

	public ForumSection getCurrentSection() {
		return currentSection;
	}

	public void setCurrentSection(ForumSection currentSection) {
		this.currentSection = currentSection;
	}

	public boolean isAddSectionShown() {
		return addSectionShown;
	}

	public void setAddSectionShown(boolean addSectionShown) {
		this.addSectionShown = addSectionShown;
	}

	public ForumSection getAddingSection() {
		return addingSection;
	}

	public void setAddingSection(ForumSection addingSection) {
		this.addingSection = addingSection;
	}

	public ForumSubject getAddingSubject() {
		return addingSubject;
	}

	public void setAddingSubject(ForumSubject addingSubject) {
		this.addingSubject = addingSubject;
	}
	
	
}
