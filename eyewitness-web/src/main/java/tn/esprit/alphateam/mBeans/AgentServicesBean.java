package tn.esprit.alphateam.mBeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;
import tn.esprit.alphateam.eyewitness.statistics.NumberOfSubscribersByDate;
import tn.esprit.alphateam.eyewitness.statistics.NumberOfVisitorsByDate;
import tn.esprit.alphateam.eyewitness.statistics.RatingByWitnessCard;

@ManagedBean
@ViewScoped
public class AgentServicesBean implements Serializable {
	private static final long serialVersionUID = 2110677120615209480L;

	private String pageDataAsImageUrl;
	private WitnessCard currentCard = new WitnessCard();
	private LineChartModel numberOfSubscribersByDateChart;
	private BarChartModel numberOfVisitorsByDateChart;
	private HorizontalBarChartModel ratingsChart;
	
	@ManagedProperty("#{userServicesBean}")
	private UserServicesBean userServicesBean;

	@EJB
	private WitnessCardServicesLocal witnessCardServicesLocal;
	
	@EJB
	private UserServicesLocal userServicesLocal;

	@PostConstruct
	public void init() {
		currentCard = userServicesBean.getCurrentUser().getWitnessCardManaged().get(0);
		createNumberOfSubscribersLineChart();
		createBarModel();
		createHorizontalBarModel();
	}
	
	public void sendStatisticsByEmail() {
		userServicesLocal.sendEmailToUser("The statistics and data relative to the witness card is attached !", "Statistics from Eyewitness", "ahmed.moalla@esprit.tn", pageDataAsImageUrl);
	}
	
	private void createHorizontalBarModel() {
        ratingsChart = new HorizontalBarChartModel();
 
        ChartSeries series = new ChartSeries();
        List<RatingByWitnessCard> list = witnessCardServicesLocal.findNumberOfRatingsByWitnessCardId(currentCard.getId());
        
        int size = list.size();
        if (size < 5) {
        	int i = 1;
        	for (int j = 0; j < list.size(); j++) {
        		RatingByWitnessCard c = list.get(j);
        		if (c.getRating() != i) {
        			series.set(i, 0);
        			j--;
        		}else {
        			series.set(c.getRating(), c.getNumber());
        		}
        		i++;
        	}
        } 

        ratingsChart.addSeries(series);
        ratingsChart.setTitle("Ratings");
        
        Axis xAxis = ratingsChart.getAxis(AxisType.X);
        xAxis.setLabel("Number of users");
         
        Axis yAxis = ratingsChart.getAxis(AxisType.Y);
        yAxis.setLabel("Ratings");        
    }

	private void createBarModel() {
		
		numberOfVisitorsByDateChart = new BarChartModel();

		ChartSeries series = new ChartSeries();
		List<NumberOfVisitorsByDate> list = witnessCardServicesLocal.findNumberOfVisitorByDateByWitnessCardId(currentCard.getId());
		
		for (NumberOfVisitorsByDate s : list) {
			series.set(s.getDate().toString(), s.getNumberOfVisitors());
		}
		numberOfVisitorsByDateChart.addSeries(series);
		numberOfVisitorsByDateChart.setTitle("Number of visitors by date");
		
		Axis yAxis = numberOfVisitorsByDateChart.getAxis(AxisType.Y);
		yAxis.setLabel("Number of visitors");

		DateAxis axis = new DateAxis();
		axis.setTickAngle(-50);
		axis.setTickFormat("%b %#d, %y");

		numberOfVisitorsByDateChart.getAxes().put(AxisType.X, axis);
	}

	public void showWitnessCard() {
		createNumberOfSubscribersLineChart();
	}

	public WitnessCard getCurrentCard() {
		return currentCard;
	}

	public void setCurrentCard(WitnessCard currentCard) {
		this.currentCard = currentCard;
	}

	public LineChartModel getNumberOfSubscribersByDateChart() {
		return numberOfSubscribersByDateChart;
	}

	public void setNumberOfSubscribersByDateChart(LineChartModel numberOfSubscribersByDateChart) {
		this.numberOfSubscribersByDateChart = numberOfSubscribersByDateChart;
	}

	private void createNumberOfSubscribersLineChart() {
		numberOfSubscribersByDateChart = new LineChartModel();
		LineChartSeries series = new LineChartSeries();
		List<NumberOfSubscribersByDate> list = witnessCardServicesLocal
				.findNumberOfSubscribersByDateByWitnessCardId(currentCard.getId());

		for (NumberOfSubscribersByDate s : list) {
			series.set(s.getDate().toString(), s.getNbSubscribers());
		}

		numberOfSubscribersByDateChart.addSeries(series);
		numberOfSubscribersByDateChart.setTitle("Number of subscribers by date");
		numberOfSubscribersByDateChart.setZoom(true);
		numberOfSubscribersByDateChart.getAxis(AxisType.Y).setLabel("Number of subscribers");
		DateAxis axis = new DateAxis();
		axis.setTickAngle(-50);
		axis.setTickFormat("%b %#d, %y");

		numberOfSubscribersByDateChart.getAxes().put(AxisType.X, axis);
	}

	public UserServicesBean getUserServicesBean() {
		return userServicesBean;
	}

	public void setUserServicesBean(UserServicesBean userServicesBean) {
		this.userServicesBean = userServicesBean;
	}

	public BarChartModel getNumberOfVisitorsByDateChart() {
		return numberOfVisitorsByDateChart;
	}

	public void setNumberOfVisitorsByDateChart(BarChartModel numberOfVisitorsByDateChart) {
		this.numberOfVisitorsByDateChart = numberOfVisitorsByDateChart;
	}

	public HorizontalBarChartModel getRatingsChart() {
		return ratingsChart;
	}

	public void setRatingsChart(HorizontalBarChartModel ratingsChart) {
		this.ratingsChart = ratingsChart;
	}


	public String getPageDataAsImageUrl() {
		return pageDataAsImageUrl;
	}

	public void setPageDataAsImageUrl(String pageDataAsImageUrl) {
		this.pageDataAsImageUrl = pageDataAsImageUrl;
	}
}
