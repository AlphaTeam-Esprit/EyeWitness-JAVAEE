package tn.esprit.alphateam.mBeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.esprit.alphateam.eyewitness.persistence.BadWord;
import tn.esprit.alphateam.eyewitness.services.interfaces.BadWordsServicesLocal;

@ManagedBean
@RequestScoped
public class BadWordDictionnaryBean {
	@EJB
	BadWordsServicesLocal badWordServicesLocal;

	//// *****
	String badwordText = "";
	List<BadWord> badwords = null ; 
	
	@PostConstruct
	public void init(){
		badwords = badWordServicesLocal.getAllBadWords();
	}
	

	public String doaddBadWOrd() {
		BadWord badWord = new BadWord();
		badWord.setText(badwordText);
		badWordServicesLocal.addBadWord(badWord);

		return "/pages/secure/admin/badwordsmanagement?faces-redirect=true";

	}
	
	
	
	
	
	
	public String doRemoveBadWord (BadWord badword){
		
		badWordServicesLocal.deleteBadWord(badword);
		
		return "/pages/secure/admin/badwordsmanagement?faces-redirect=true";
	}
	
	
	
	
	
	
	
	
	

	public List<BadWord> getBadwords() {
		return badwords;
	}


	public void setBadwords(List<BadWord> badwords) {
		this.badwords = badwords;
	}


	public String getBadwordText() {
		return badwordText;
	}

	public void setBadwordText(String badwordText) {
		this.badwordText = badwordText;
	}
}
