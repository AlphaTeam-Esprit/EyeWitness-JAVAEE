package tn.esprit.alphateam.mBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

import tn.esprit.alphateam.eyewitness.persistence.User;
import tn.esprit.alphateam.eyewitness.persistence.WitnessCard;
import tn.esprit.alphateam.eyewitness.services.interfaces.UserServicesLocal;
import tn.esprit.alphateam.eyewitness.services.interfaces.WitnessCardServicesLocal;




@ManagedBean
public class ChartView implements Serializable {
	private BarChartModel barModel;
	private HorizontalBarChartModel horizontalBarModel;
	
	@EJB
	private UserServicesLocal userService;
	@EJB
	private WitnessCardServicesLocal witnesscardService;
	
	private long maxUserComment = 0;
	private long maxWitnessComment = 0;
	
	@PostConstruct
	public void init() {
		createBarModels();
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public HorizontalBarChartModel getHorizontalBarModel() {
		return horizontalBarModel;
	}

	private BarChartModel initBarModel() {
		BarChartModel model = new BarChartModel();

		ChartSeries users = new ChartSeries();
		//users.setLabel("Users");
		
		maxUserComment = 0;
		for (User user : userService.findAllUsers()) {
			long commentCount = userService.getCommentCount(user.getId());
			if(commentCount > maxUserComment){
				maxUserComment = commentCount;
			}
			users.set(user.getFirstName() + " " + user.getLastName(), 
					commentCount);
		}
		

		model.addSeries(users);

		return model;
	}

	private void createBarModels() {
		createBarModel();
		createHorizontalBarModel();
	}

	private void createBarModel() {
		barModel = initBarModel();

		barModel.setTitle("Active User");
		barModel.setLegendPosition("ne");

		Axis xAxis = barModel.getAxis(AxisType.X);
		xAxis.setLabel("Users");

		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setLabel("Comment Count");
		yAxis.setMin(0);
		yAxis.setMax(maxUserComment + 3);
	}

	private void createHorizontalBarModel() {
		horizontalBarModel = new HorizontalBarChartModel();

		ChartSeries witnessChart = new ChartSeries();
		
		maxWitnessComment = 0; 
		
		for (WitnessCard witnesscard : witnesscardService.listWitnessCards()) {
			long witnessCount = witnesscardService.getCommentCount(witnesscard.getId());
			if(witnessCount > maxWitnessComment){
				maxWitnessComment = witnessCount;
			}
			witnessChart.set(witnesscard.getName(), witnessCount);
		}
		
		/*ChartSeries boys = new ChartSeries();
		boys.setLabel("Boys");
		boys.set("2004", 50);
		boys.set("2005", 96);
		boys.set("2006", 44);
		boys.set("2007", 55);
		boys.set("2008", 25);

		ChartSeries girls = new ChartSeries();
		girls.setLabel("Girls");
		girls.set("2004", 52);
		girls.set("2005", 60);
		girls.set("2006", 82);
		girls.set("2007", 35);
		girls.set("2008", 120);

		horizontalBarModel.addSeries(boys);
		horizontalBarModel.addSeries(girls);
*/
		horizontalBarModel.addSeries(witnessChart);
		horizontalBarModel.setTitle("Pourcentage of witnesscard");
		horizontalBarModel.setLegendPosition("e");
		horizontalBarModel.setStacked(true);

		Axis xAxis = horizontalBarModel.getAxis(AxisType.X);
		xAxis.setLabel("Witnesscard");
		xAxis.setMin(0);
		xAxis.setMax(maxWitnessComment + 3);

		Axis yAxis = horizontalBarModel.getAxis(AxisType.Y);
		yAxis.setLabel("Comment count");
	}

}